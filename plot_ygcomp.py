# This script plots a comparison of the cluster_slug and yggdrasil results

from astropy.io import ascii
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import os
import os.path as osp
from slugpy import read_filter
from slugpy.int_tabulated import int_tabulated, int_tabulated2

# yggdrasil files
ygfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
           'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
           'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
distmod = [27.68, 27.72, 29.98]

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Specify cluster_slug models to use
csfile = 'modp020_kroupalim_MW_phi0.50'
fitdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']
ngal = len(galnames)
msfid = 2
tsfid = 0.5

# Read the Yggdrasil estimates, stripping out things that are not
# classified as well-detected clusters
cid_yg = []
age = []
mass = []
ebv = []
chi2 = []
chi2q = []
photerrmean = []
idxcl = []
for nidx, ygf in zip(nuc_cl, ygfiles):

    # Read data and filter for true clusters; also filter out the
    # nuclear cluster
    data = ascii.read(ygf)
    classification = data['col34']
    idx = np.logical_and(classification > 0, classification < 3.5)
    cid_tmp = list(data['col1'])
    if nidx in cid_tmp:
        classification[cid_tmp.index(nidx)] = False
    nidx = np.sum(idx)

    # Prepare storage
    age_tmp = np.zeros((3,nidx))
    mass_tmp = np.zeros((3,nidx))
    ebv_tmp = np.zeros((3,nidx))
    photerr_tmp = np.zeros((nidx, 5))

    # Store
    cid_tmp = data['col1'][idx]
    age_tmp[1,:] = data['col17'][idx]
    age_tmp[0,:] = data['col19'][idx]
    age_tmp[2,:] = data['col18'][idx]
    mass_tmp[1,:] = data['col20'][idx]
    mass_tmp[0,:] = data['col22'][idx]
    mass_tmp[2,:] = data['col21'][idx]
    ebv_tmp[1,:] = data['col23'][idx]
    ebv_tmp[0,:] = data['col24'][idx]
    ebv_tmp[2,:] = data['col25'][idx]
    chi2_tmp = np.array(data['col31'][idx])
    chi2q_tmp = np.array(data['col32'][idx])
    for i in range(5):
        photerr_tmp[:,i] = data['col{:d}'.format(2*i+7)][idx]
    photerr_tmp[photerr_tmp > 99] = 0.0  # Null out non-detections
    photerrmean_tmp = np.sqrt(np.sum(photerr_tmp**2, axis=1)) / \
                      np.sqrt(np.sum(photerr_tmp != 0.0, axis=1))
    cid_yg.append(cid_tmp)
    age.append(age_tmp)
    mass.append(mass_tmp)
    ebv.append(ebv_tmp)
    chi2.append(chi2_tmp)
    chi2q.append(chi2q_tmp)
    idxcl.append(idx)
    photerrmean.append(photerrmean_tmp)

# Read data
csfit = []
for i in range(len(galnames)):
    fname = osp.join(fitdir,
                     galnames[i]+'_'+osp.basename(csfile) + \
                     '_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.format(msfid, tsfid))
    hdulist = fits.open(fname)
    logm = hdulist[1].data['log_m']
    logt = hdulist[1].data['log_t']
    AV = hdulist[1].data['AV']
    cid = hdulist[2].data['Cluster_ID'][idxcl[i]]
    mpdf = hdulist[2].data['Mass_PDF'][idxcl[i]]
    tpdf = hdulist[2].data['Age_PDF'][idxcl[i]]
    AVpdf = hdulist[2].data['AV_PDF'][idxcl[i]]
    dist = hdulist[2].data['Phot_dist'][idxcl[i]]
    dist_norm = hdulist[2].data['Phot_dist_norm'][idxcl[i]]
    hdulist.close()

    # Compute percentiles
    mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
    logm_pct = np.array([
        logm[np.argmax(np.greater(mpdfsum, 0.16), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.84), axis=1)]])
    tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
    logt_pct = np.array([
        logt[np.argmax(np.greater(tpdfsum, 0.16), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.84), axis=1)]])
    AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
    AV_pct = np.array([
        AV[np.argmax(np.greater(AVpdfsum, 0.16), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.84), axis=1)]])

    # Save data
    csfit.append(
        { 'logm' : logm, 'logt' : logt, 'AV' : AV, 
          'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
          'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
          'AV_pct' : AV_pct, 'dist' : dist, 'dist_norm' : dist_norm,
          'id' : cid })

# Read extinction curves and filter responses
mwext = np.loadtxt(os.path.join(os.environ['SLUG_DIR'], 'lib', 'extinct',
                                'MW_EXT_SLUG.dat'))
sbext = np.loadtxt(os.path.join(os.environ['SLUG_DIR'], 'lib', 'extinct',
                                'SB_ATT_SLUG.dat'))
filterv = read_filter('Johnson_V')
filterb = read_filter('Johnson_B')

# Convert wavelength to frequency, and get normalization factor to
# make A_V = 1
c = 2.9979e10
numw = c/(1e-8*mwext[::-1,0])
nusb = c/(1e-8*sbext[::-1,0])
kappamw = mwext[::-1,1]
kappasb = sbext[::-1,1]
nuv = c/(1.0e-8*filterv.wl[::-1])
nub = c/(1.0e-8*filterb.wl[::-1])
responsev = filterv.response[::-1]
responseb = filterb.response[::-1]
normmw = np.log(100.0) / 5.0 * int_tabulated(nuv, responsev) / \
         int_tabulated2(numw, kappamw, nuv, responsev)
normsb = np.log(100.0) / 5.0 * int_tabulated(nuv, responsev) / \
         int_tabulated2(nusb, kappasb, nuv, responsev)

# Get A_B and E(B-V) corresponding to A_V = 1
ABmw = normmw * 5.0/np.log(100) * \
       int_tabulated2(numw, kappamw, nub, responseb) / \
       int_tabulated(nub, responseb)
ABsb = normsb * 5.0/np.log(100) * \
       int_tabulated2(nusb, kappasb, nub, responseb) / \
       int_tabulated(nub, responseb)
EBVmw = ABmw - 1.0
EBVsb = ABsb - 1.0

# Plot yg versus cs
fig = plt.figure(1, figsize=(5,12))
plt.clf()
mra=[2,7]
tra=[5.5,10.5]
avra=[0,3]
skip=0.1
marker = ['o', 's', '^']
color = ['b', 'g', 'r']

# Plot masses
ax = fig.add_subplot(3,1,1)

# Loop over galaxies
ptmp=[]
for i in range(ngal-1,-1,-1):

    # Grab the data and sort it
    xdata = csfit[i]['logm_pct'][1,:]
    ydata = np.log10(mass[i][1,:])
    xerr = np.array([csfit[i]['logm_pct'][1,:]-
                     csfit[i]['logm_pct'][0,:],
                     csfit[i]['logm_pct'][2,:]-
                     csfit[i]['logm_pct'][1,:]])
    yerr = np.array([np.log10(mass[i][1,:])-
                     np.log10(mass[i][0,:]),
                     np.log10(mass[i][2,:])-
                     np.log10(mass[i][1,:])])
    idx = np.argsort(xdata)
    xdata = xdata[idx]
    ydata = ydata[idx]
    xerr = xerr[:,idx]
    yerr = yerr[:,idx]

    # Plot medians
    p1,=ax.plot(xdata, ydata,
            color[i]+marker[i], label=galnames[i])
    ptmp.append(p1)

    # Plot errorbars on some points
    idx = [0]
    for l in range(len(xdata)):
        if xdata[l] - xdata[idx[-1]] > skip:
            idx.append(l)
    ax.errorbar(xdata[idx], ydata[idx], xerr = xerr[:,idx], 
                yerr = yerr[:,idx],
                fmt='none', ecolor=color[i])

# Form average over all galaxies
nclbin = np.zeros(5, dtype='int')
mcsavg = np.zeros(5)
mygavg = np.zeros(5)
for i in range(5):
    for j in range(ngal):
        idx = np.where(
            np.logical_and(csfit[j]['logm_pct'][1,:] > i+2,
                           csfit[j]['logm_pct'][1,:] <= i+3))[0]
        nclbin[i] = nclbin[i] + len(idx)
        mcsavg[i] = mcsavg[i] + np.sum(csfit[j]['logm_pct'][1,idx])
        mygavg[i] = mygavg[i] + np.sum(np.log10(mass[j][1,idx]))
mcsavg = mcsavg / nclbin
mygavg = mygavg / nclbin
p1,=ax.plot(mcsavg, mygavg, 'yo', ms=16, mew=3, mec='k', alpha=0.75)
ptmp.append(p1)

ax.plot(mra, mra, 'k--')
ax.set_xlim(mra)
ax.set_ylim(mra)
ax.set_ylabel('$\log\,(M/M_\odot)$ (Yggdrasil)')
ax.set_xlabel('$\log\,(M/M_\odot)$ (cluster_slug)', labelpad=8)
ax.legend(ptmp, gallabels+['Mean'], loc='upper left', numpoints=1)


# Plot ages
ax = fig.add_subplot(3,1,2)

# Loop over galaxies
for i in range(ngal-1,-1,-1):

    # Grab the data and sort it
    xdata = csfit[i]['logt_pct'][1,:]
    ydata = np.log10(age[i][1,:])
    xerr = np.array([csfit[i]['logt_pct'][1,:]-
                     csfit[i]['logt_pct'][0,:],
                     csfit[i]['logt_pct'][2,:]-
                     csfit[i]['logt_pct'][1,:]])
    yerr = np.array([np.log10(age[i][1,:])-
                     np.log10(age[i][0,:]),
                     np.log10(age[i][2,:])-
                     np.log10(age[i][1,:])])
    idx = np.argsort(xdata)
    xdata = xdata[idx]
    ydata = ydata[idx]
    xerr = xerr[:,idx]
    yerr = yerr[:,idx]

    # Plot medians
    ax.plot(xdata, ydata,
            color[i]+marker[i], label=galnames[i])

    # Plot errorbars on some points
    idx = [0]
    for l in range(len(xdata)):
        if xdata[l] - xdata[idx[-1]] > skip:
            idx.append(l)
    ax.errorbar(xdata[idx], ydata[idx], xerr = xerr[:,idx], 
                yerr = yerr[:,idx],
                fmt='none', ecolor=color[i])

# Form average over all galaxies
nclbin = np.zeros(4, dtype='int')
tcsavg = np.zeros(4)
tygavg = np.zeros(4)
for i in range(4):
    for j in range(ngal):
        idx = np.where(
            np.logical_and(csfit[j]['logt_pct'][1,:] > i+6,
                           csfit[j]['logt_pct'][1,:] <= i+7))[0]
        nclbin[i] = nclbin[i] + len(idx)
        tcsavg[i] = tcsavg[i] + np.sum(csfit[j]['logt_pct'][1,idx])
        tygavg[i] = tygavg[i] + np.sum(np.log10(age[j][1,idx]))
tcsavg = tcsavg / nclbin
tygavg = tygavg / nclbin
ax.plot(tcsavg, tygavg, 'yo', ms=16, mew=3, mec='k', alpha=0.75)

ax.plot(tra, tra, 'k--')
ax.set_xlim(tra)
ax.set_ylim(tra)
ax.set_ylabel('$\log\,(T/\mathrm{yr})$ (Yggdrasil)')
ax.set_xlabel('$\log\,(T/\mathrm{yr})$ (cluster_slug)', labelpad=8)


# Plot AV
ax = fig.add_subplot(3,1,3)

# Loop over galaxies
for i in range(ngal-1,-1,-1):

    # Grab the data and sort it
    xdata = csfit[i]['AV_pct'][1,:]
    ydata = ebv[i][1,:]/EBVmw
    xerr = np.array([csfit[i]['AV_pct'][1,:]-
                     csfit[i]['AV_pct'][0,:],
                     csfit[i]['AV_pct'][2,:]-
                     csfit[i]['AV_pct'][1,:]])
    yerr = np.array([ebv[i][1,:]/EBVmw-
                     ebv[i][0,:]/EBVmw,
                     ebv[i][2,:]/EBVmw-
                     ebv[i][1,:]/EBVmw])
    idx = np.argsort(xdata)
    xdata = xdata[idx]
    ydata = ydata[idx]
    xerr = xerr[:,idx]
    yerr = yerr[:,idx]

    # Plot medians
    ax.plot(xdata, ydata,
            color[i]+marker[i], label=galnames[i])

    # Plot errorbars on some points
    idx = [0]
    for l in range(len(xdata)):
        if xdata[l] - xdata[idx[-1]] > skip:
            idx.append(l)
    ax.errorbar(xdata[idx], ydata[idx], xerr = xerr[:,idx], 
                yerr = yerr[:,idx],
                fmt='none', ecolor=color[i])

ax.plot(avra, avra, 'k--')
ax.set_xlim(avra)
ax.set_ylim(avra)
ax.set_ylabel('$A_V$ (Yggdrasil)')
ax.set_xlabel('$A_V$ (cluster_slug)', labelpad=8)

# Adjust
plt.subplots_adjust(bottom=0.06, top=0.97, right=0.93, left=0.15)

# Save
plt.savefig('ygcomp.pdf')


# Now plot comparison of errors / fit quality
fig = plt.figure(2, figsize=(8,6))
plt.clf()
gspec = gs.GridSpec(2, 2, width_ratios = [5,1],
                    height_ratios = [1,5])

# Plot points where Q >= ycut
ycut = 1e-3
ax = plt.subplot(gspec[2])
ptmp=[]
for i in range(ngal-1,-1,-1):
    idx = np.where(chi2q[i] >= ycut)[0]
    p1,=ax.plot(csfit[i]['dist_norm'][idx,4], chi2q[i][idx],
                color[i]+marker[i])
    ptmp.append(p1)

# Plot remaining points as limits
for i in range(ngal-1,-1,-1):
    idx = np.where(chi2q[i] < ycut)[0]
    ax.errorbar(csfit[i]['dist_norm'][idx,4], np.ones(len(idx))*0.8*ycut,
                uplims=True, yerr=0.5*ycut, fmt='none', ecolor=color[i])

# Change axes, add legend
ax.legend(ptmp, gallabels, loc='upper right', numpoints=1)
ax.set_yscale('log')
ax.set_ylim([0.2*ycut, 2])
ax.set_xlim([0,4.5])
ax.set_xticks(np.arange(0, 4.5, 0.5))
ax.set_xlabel(r'$D_5^{\mathrm{norm}}$ (cluster_slug)', labelpad=12)
ax.set_ylabel(r'$Q(\chi^2)$ (Yggdrasil)')

# Bin data in chi2q
chi2qtot = chi2q[0]
for i in range(1,ngal):
    chi2qtot = np.append(chi2qtot, chi2q[i])
chi2qhist, chi2qbin \
    = np.histogram(chi2qtot, bins=10.**np.arange(np.log10(ycut), 0.01, 0.25))

# Bin data in D5norm
d5normtot = np.copy(csfit[0]['dist_norm'][:,4])
for i in range(1,ngal):
    d5normtot = np.append(d5normtot, csfit[i]['dist_norm'][:,4])
d5hist, d5bin \
    = np.histogram(d5normtot, bins=np.arange(0, 5, 0.25))

# Add histogram of D5norm values, and line showing theoretical
# expection
ax = plt.subplot(gspec[0])
ax.bar(d5bin[:-1], d5hist/float(np.sum(d5hist)), width=d5bin[1:]-d5bin[:-1],
       color='y', lw=2)
l=6.
m=1.
nex = l*d5bin**m
d5cdf = 1.0 - (1+nex+nex**2/2+nex**3/6+nex**4/24)*np.exp(-nex)
ax.plot(0.5*(d5bin[1:]+d5bin[:-1]), d5cdf[1:]-d5cdf[:-1], 'k', lw=2)
ax.set_xticklabels([])
ax.set_xlim([0,4.5])
ax.set_yticks([0, 0.1, 0.2])
ax.set_ylabel('Frequency')

# Add histogram of chi2q values, including extra bar for values below
# the cut, and line showing theoretical expectation
ax = plt.subplot(gspec[3])
ax.barh(chi2qbin[:-1], chi2qhist/float(len(chi2qtot)), 
        height=chi2qbin[1:]-chi2qbin[:-1], color='y', lw=2)
ax.barh(0.2**0.25*ycut, np.sum(chi2qtot < ycut)/float(len(chi2qtot)),
        height=(0.2**0.75-0.2**0.25)*ycut, color='y', lw=2,
        hatch='//')
ax.plot(chi2qbin[1:]-chi2qbin[:-1],
        np.sqrt(chi2qbin[:-1]*chi2qbin[1:]),
        'k', lw=3)
ax.set_xlim([0, 0.35])
ax.set_ylim([0.2*ycut, 2])
ax.set_yscale('log')
ax.set_xticks([0, 0.1, 0.2, 0.3])
ax.set_yticklabels([])
ax.set_xlabel('Frequency')

# Save
plt.savefig('cs_yg_errordist.pdf')
