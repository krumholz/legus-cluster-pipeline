# This script plots the joint mass-age distribution for the whole
# cluster population

import numpy as np
from numpy.random import randint
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii
import glob

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
distmod = [27.68, 27.72, 29.98]

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Specify cluster_slug models to use
csfile = '/Users/krumholz/Data/legus/modp020_kroupalim_MW_phi0.50'
libdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1/'
libname = 'pad_020_kroupa_MW'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']

# Specify the priors to use
layout = [4,2]
mslist = [1.0, 1.0, 1.0, 2.0, 2.0, 2.0]
tslist = [0.0, 0.5, 1.0, 0.0, 0.5, 1.0]
msfid = 2.0
tsfid = 0.5
fididx = 4

# Go through photometry files to find high confidence clusters
classidx = []
ra = []
dec = []
ygmass = []
ygage = []
for dmod, nucidx, pf in zip(distmod, nuc_cl, photfiles):

    # Read classification
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))

    # Filter out the nuclear cluster
    cid = list(data['col1'])
    if nucidx in cid:
        classidx[-1][cid.index(nucidx)] = False

    # Get RA and DEC to remove duplicates
    ra_tmp = np.array(data['col4'])
    dec_tmp = np.array(data['col5'])
    for r, d in zip(ra, dec):
        dup = np.logical_and(np.in1d(ra_tmp, r), np.in1d(dec_tmp, d))
        classidx[-1] = np.logical_and(classidx[-1], np.logical_not(dup))

    # Store
    ygage.append(data['col17'][classidx[-1]])
    ygmass.append(data['col20'][classidx[-1]])
    ra.append(ra_tmp[classidx[-1]])
    dec.append(dec_tmp[classidx[-1]])

# Put all yggdrasil fits into a single array
ygmall=np.zeros(0)
ygtall=np.zeros(0)
for ygm, ygt in zip(ygmass, ygage):
    ygmall=np.append(ygmall, ygm)
    ygtall=np.append(ygtall, ygt)

# Read the high-quality clusters from the 3D PDF file
cspdfs = []
for ms, ts in zip(mslist, tslist):
    ncl = 0
    cs2d = []
    for cidx, gname in zip(classidx, galnames):

        # Get file name to read
        fname = osp.join(libdir, gname+'_'+osp.basename(csfile) +
                         '_ms{:3.1f}_ts{:3.1f}_pdf_3D.fits'.format(
                             ms, ts))

        # Open file
        hdulist = fits.open(fname)

        # Read the mass - age - AV grid
        logm = hdulist[1].data['log_m']
        logt = hdulist[1].data['log_t']
        AV = hdulist[1].data['AV']

        # Read the PDFs and reshape them
        pdf = hdulist[2].data['pdf']
        pdf = pdf.reshape((pdf.shape[0], len(logm), len(logt), len(AV)))

        # Close file
        hdulist.close()

        # Throw out low-quality candidates
        pdf = pdf[cidx,:,:,:]
        ncl = ncl + len(pdf)

        # Sum along the AV dimension
        mtpdf = np.sum(pdf, axis=3)

        # Renormalize
        dlogm = logm[1] - logm[0]
        dlogt = logt[1] - logt[0]
        mtpdf = mtpdf / (np.sum(mtpdf[0,:,:])*dlogm*dlogt)

        # Save
        cs2d.append(mtpdf)

    # Save
    cspdfs.append(cs2d)

# Sum all fields
mtpdf = np.zeros((len(cspdfs), len(logm), len(logt)))
mtgal = []
for i, cs2d in enumerate(cspdfs):
    for cs in cs2d:
        mtgal.append(np.mean(cs, axis=0))
        mtpdf[i,:,:] = mtpdf[i,:,:] + mtgal[-1]*float(len(cs))/ncl

# Make a 2D histogram of the yggdrasil masses and ages
medges = np.arange(2.0, 8.001, 0.25)
tedges = np.arange(5.0, 10.5001, 0.25)
mctr = 0.5*(medges[1:]+medges[:-1])
tctr = 0.5*(tedges[1:]+tedges[:-1])
mtpdf_yg = np.histogram2d(np.log10(ygmall), np.log10(ygtall), 
                          bins=[medges, tedges], normed=True)[0]

# Make grids for contours
mtgrd = np.meshgrid(logm, logt, indexing='ij')
mtgrd_yg = np.meshgrid(mctr, tctr, indexing='ij')

# Plot
fig = plt.figure(1, figsize=(6,12))
plt.clf()

# Plot cs results for different priors
for i in range(len(mtpdf)):
    ax = fig.add_subplot(layout[0],layout[1],i+1)
    plt.imshow(np.transpose(np.log10(mtpdf[i,:,:])), origin='lower',
               extent=(logm[0], logm[-1], logt[0], logt[-1]),
               cmap = 'Blues', vmin=-4, vmax=0, aspect='auto')
    plt.contour(mtgrd[0], mtgrd[1], np.log10(mtpdf[fididx,:,:]), 
                levels=np.arange(-3,0.01,0.5),
                colors='k', linestyles='dashed')
    plt.contour(mtgrd[0], mtgrd[1], np.log10(mtpdf[i,:,:]), 
                levels=np.arange(-3,0.01,0.5),
                colors='k', linestyles='solid')
    if (mslist[i] != msfid) or (tslist[i] != tsfid):
        plt.text(2.2, 9.75, r'cluster_slug')
    else:
        plt.text(2.2, 9.75, r'cluster_slug (fid.)')
    plt.text(2.2, 9.35, r'$\beta=-{:3.1f}$'.format(mslist[i]))
    plt.text(2.2, 9.0, r'$\gamma=-{:3.1f}$'.format(tslist[i]))
    plt.xlim([2,7])
    plt.ylim([5.0,10.18])
    if i < len(mtpdf)-1:
        ax.get_xaxis().set_ticklabels([])
    else:
        plt.xlabel(r'$\log\,M$ [$M_\odot$]')
        ax.get_xaxis().set_ticks([3,4,5,6])
    if i % layout[1] == 0:
        ax.get_yaxis().set_ticks([6,7,8,9,10])
    else:
        ax.get_yaxis().set_ticklabels([])

# Plot yg results
ax = fig.add_subplot(layout[0],layout[1],len(mtpdf)+1)
plt.imshow(np.transpose(np.log10(mtpdf_yg+1e-6)), origin='lower',
           extent=(medges[0], medges[-1], tedges[0], tedges[-1]),
           cmap = 'Blues', vmin=-4, vmax=0, aspect='auto',
           interpolation='nearest')
plt.contour(mtgrd[0], mtgrd[1], np.log10(mtpdf[fididx,:,:]), 
            levels=np.arange(-3,0.01,0.5),
            colors='k', linestyles='dashed')
plt.text(2.2, 9.75, r'Yggdrasil')
plt.xlim([2,7])
ax.get_xaxis().set_ticks([2,3,4,5,6])
plt.ylim([5,10.2])
plt.xlabel(r'$\log\,M$ [$M_\odot$]')

# Adjustment and overall y axis label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel('$\log\,T$ [yr]', labelpad=14)
plt.subplots_adjust(wspace=0, hspace=0, bottom=0.07, top=0.96, right=0.84)

# Add colorbar
axcbar = fig.add_axes((0.84, 0.07, 0.04, 0.96-0.07))
norm = mpl.colors.Normalize(vmin=-4, vmax=0)
cbar = mpl.colorbar.ColorbarBase(axcbar, cmap=cm.Blues, norm=norm)
cbar.set_label('log PDF')

# Save
plt.savefig('joint_age_mass.pdf')
