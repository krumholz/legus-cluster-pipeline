# Script to process all the LEGUS photometry for a specified set of
# galaxies thorough cluster_slug to produce posterior PDFs.

import os.path as osp
import subprocess

# Input photometry files
files = [ 'ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab',
	  'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
	  'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab' ]
filters = [ 
    ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
     'ACS_F555W', 'ACS_F814W']
    , ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
       'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']
    , ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'ACS_F435W',
       'WFC3_UVIS_F555W', 'ACS_F814W']
]

# Distance mods for photometric files
dmod = [
    27.68
    , 27.72
    , 29.98
]

# cluster_slug library names
#libdir = '/pfs/krumholz/cluster_slug/lib/'
libdir = '/Users/krumholz/Data/legus/libs/'
cslib = [
    'modp020_kroupalim_MW_phi0.50'
   , 'modp020_kroupalim_SB_phi0.50'
   , 'modp004_kroupalim_MW_phi0.50'
   , 'modp008_kroupalim_MW_phi0.50'
   , 'modp020_chabrier_MW_phi0.50'
   , 'Z0140v00_kroupalim_MW_phi0.50'
   , 'modp020_kroupalim_MW_phi0.50'
]
no_nebular = [
    False
    , False
    , False
    , False
    , False
    , False
    , True
]

# Number of processors to use; set to -1 to use all available
nproc = -1

# Number of dimensions in the posterior PDF (i.e., do we want to
# marginalize over anything)
ndim = 1

# Directory into which to write output
#outdir = '/pfs/krumholz/cluster_slug/legus/fits/'
outdir = '/Users/krumholz/Projects/legus_stochastic/legus_clusters/'

# Priors on dN/dM and dN/dT
mass_slope_prior = [-2, -1]
age_slope_prior = [0, -0.5, -1]

# Loop over input files and distance moduli
for f, d, filt in zip(files, dmod, filters):

    # Loop over model libraries
    for lib, no_neb in zip(cslib, no_nebular):

        # Loop over priors
        for mslope in mass_slope_prior:
            for tslope in age_slope_prior:

                # Construct output name for this case
                if no_neb:
		    outfile = f.split('_')[0] + \
                              '_{:s}_noneb_ms{:3.1f}_ts{:3.1f}'.\
                              format(osp.basename(lib), -mslope, -tslope)
		else:
		    outfile = f.split('_')[0] + \
                              '_{:s}_ms{:3.1f}_ts{:3.1f}'.\
                              format(osp.basename(lib), -mslope, -tslope)
                if ndim == 1:
                    outfile = outfile + '_pdf_1D'
                elif ndim == 2:
                    outfile = outfile + '_pdf_2D'
                else:
                    outfile = outfile + '_pdf_3D'
                outfile = osp.join(outdir, outfile)

                # Run this model
                cmd = ('python getpdfs.py {:s} {:s} --distmod {:f} ' +
                       '--ageslope {:f} --massslope {:f} ' +
                       '--filters {:s} {:s} {:s} {:s} {:s} ' +
                       '--outfile {:s} --ndim {:d} --verbose'). \
                    format(f, osp.join(libdir, lib), d, tslope, mslope, 
                           filt[0], filt[1], filt[2], filt[3], filt[4],
                           outfile, ndim)
                if nproc > 0:
                    cmd = cmd + ' --nproc {:d}'.format(nproc)
                if no_neb:
                    cmd = cmd + ' --nonebular'
                print cmd
                subprocess.call(cmd, shell=True)




