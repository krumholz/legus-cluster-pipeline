# Script to read a specified set of input photometry, use cluster_slug
# to get posteriors on part of it, and write the result to disk

import argparse
from slugpy.cluster_slug import cluster_slug
from astropy.io import ascii
from astropy.io import fits
import numpy as np
import os
import os.path as osp
import subprocess
from multiprocessing import Pool, RawArray, Value, current_process, \
    cpu_count
import ctypes
import time

# Set up default filter list
filter_default=['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
                'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']

# Parse the inputs
parser = argparse.ArgumentParser(
    description="Posterior PDF computation script")
parser.add_argument('photfile', help='input photometry file')
parser.add_argument('libname', help='name of cluster_slug library to use')
parser.add_argument('-d', '--distmod', type=float, default=0.0,
                    help='distance modulus to apply to input photometry')
parser.add_argument('-p', '--photsystem', default='Vega',
                    help='photometric system used for data')
parser.add_argument('-o', '--outfile', default=None,
                    help='Name of output file, omitting the .fits extension')
parser.add_argument('-n', '--nneighbor', type=int, default=5,
                    help='Number of nearest neighbor distances to find')
parser.add_argument('-a', '--ageslope', default=-1.0, type=float,
                    help='prior dN / dlogt ~ t^ageslope at t > 10^6.5 yr (default: 0)')
parser.add_argument('-m', '--massslope', default=-1.0, type=float,
                    help='prior dN / dlogM ~ M^massslope (default: -1)')
parser.add_argument('--batchsize', default=-1, type=int,
                    help='clusters to process per thread call')
parser.add_argument('-nd', '--ndim', default=3, type=int,
                    help='number of dimensions in posterior PDF; 1 = separate mass, age, AV PDFs, 2 = joint mass-age PDF, 3 = joint mass-age-AV PDF')
parser.add_argument('--nproc', default=-1, type=int,
                    help='number of processors to use (default: all)')
parser.add_argument('--ngrid', default=128, type=int,
                    help='size of grid on which to compute posterior PDFs')
parser.add_argument('-nn', '--nonebular', action='store_true',
                    default=False, help='ignore nebular emission')
parser.add_argument('--savetemp', default=False, action='store_true',
                    help='do not remove temporary files')
parser.add_argument('-f', '--filters', nargs='*', default=filter_default,
                    help='list of filters present in the photometric data file')
parser.add_argument('-if', '--ignorefilters', nargs='*', default=None,
                    help='list of filters NOT to use in the analysis')
parser.add_argument('-v', '--verbose', action='store_true',
                    default=False, help='verbose output')
args = parser.parse_args()

# Read the input photometry file
if args.verbose:
    print("Reading input photometry from {}...".format(args.photfile))
data = ascii.read(args.photfile)
cid = data['col1']
nc = len(cid)
nf = len(args.filters)
phot = np.zeros((nc, nf))
photerr = np.zeros((nc, nf))
detect = np.ones((nc, nf), dtype=bool)
for i in range(nf):
    phot[:,i] = data['col{:d}'.format(2*i+6)] - args.distmod
    photerr[:,i] = data['col{:d}'.format(2*i+7)]
    detect[:,i] = data['col{:d}'.format(2*i+6)] < 99.  # Non-detection flag

# Throw out filters if told to do so
filters = args.filters
if args.ignorefilters is not None:
    for f in args.ignorefilters:
        if f in filters:
            idx = filters.index(f)
            phot = np.delete(phot, idx, axis=1)
            photerr = np.delete(photerr, idx, axis=1)
            detect = np.delete(detect, idx, axis=1)
            filters.remove(f)
    nf = len(filters)

# Construct the list of filters to be used for each cluster, based on
# for which ones we have data
filt_to_use = []
for dt in detect:
    filtertmp = []
    for d, f in zip(dt, filters):
        if d:
            filtertmp.append(f)
    filt_to_use.append(filtertmp)

# Set sample density for known libraries
if osp.basename(args.libname) == 'modp020_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_kroupalim_SB_phi0.50' or \
   osp.basename(args.libname) == 'modp004_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp008_kroupalim_MW_phi0.50' or \
   osp.basename(args.libname) == 'modp020_chabrier_MW_phi0.50' or \
   osp.basename(args.libname) == 'Z0140v00_kroupalim_MW_phi0.50':
    def sample_density(physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        sden = np.ones(len(logm))
        sden[logm > 4] = sden[logm > 4] * 1.0/10.**(logm[logm > 4]-4)
        sden[logt > 8] = sden[logt > 8] * 1.0/10.**(logt[logt > 8]-8)
        return sden
else:
    if args.verbose:
        print("Unrecognized file name, using uniform sample density")
    sample_density = None

# Set priors
class priorfunc(object):
    def __init__(self, massslope, ageslope, logageflat=6.5):
        self.massslope = massslope
        self.ageslope = ageslope
        self.logtflat = logageflat
    def prior(self, physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        massprior = (10.**logm)**(self.massslope+1)
        timeprior = (10.**(logt-self.logtflat))**(self.ageslope+1)
        timeprior[logt < self.logtflat] \
            = 10.**(logt[logt < self.logtflat]-self.logtflat)
        return massprior*timeprior
prior = priorfunc(args.massslope, args.ageslope)

# Create the cluster_slug object
if args.verbose:
    print("Reading cluster_slug library...")
cs = cluster_slug(photsystem=args.photsystem,
                  libname=args.libname, priors=prior.prior, 
                  sample_density = sample_density,
                  use_nebular = not args.nonebular,
                  abstol = 1.0e-10, bw_phot = 0.1)

# Add all the filters we're going to need; do this here so that they
# will be globally shared
for f in filt_to_use:
    if f not in cs.filtersets():
        if args.verbose:
            outstr = "Constructing kernel density estimation for filters"
            for f1 in f:
                outstr = outstr + " " + f1
            print(outstr)
        cs.add_filters(f)

# Define functions suitable for parallel processing
def mpdf_1D(i):
    if args.verbose:
        with ndone.get_lock():
            print "{:s}: computing 1D posterior PDFs for cluster {:d}/{:d}".\
                format(current_process().name, ndone.value+1, ncl.value)
            ndone.value = ndone.value+1
    logm[:], mpdf[i] = cs.mpdf(
        0, phot[i][detect[i]], photerr[i][detect[i]], 
        filters=filt_to_use[i], ngrid=args.ngrid)
    logt[:], tpdf[i] = cs.mpdf(
        1, phot[i][detect[i]], photerr[i][detect[i]], 
        filters=filt_to_use[i], ngrid=args.ngrid)
    AV[:], AVpdf[i] = cs.mpdf(
        2, phot[i][detect[i]], photerr[i][detect[i]], 
        filters=filt_to_use[i], ngrid=args.ngrid)

def mpdf_2D(i):
    if args.verbose:
        with ndone.get_lock():
            print "{:s}: computing 2D posterior PDF for cluster {:d}/{:d}".\
                format(current_process().name, ndone.value+1, ncl.value)
            ndone.value = ndone.value+1
    grd[:,:,:], pdf[i] = cs.mpdf(
        [0,1], phot[i][detect[i]], photerr[i][detect[i]], 
        filters=filt_to_use[i], ngrid=args.ngrid)

def mpdf_3D(i):
    if args.verbose:
        with ndone.get_lock():
            print "{:s}: computing 3D posterior PDF for cluster {:d}/{:d}".\
                format(current_process().name, ndone.value+1, ncl.value)
            ndone.value = ndone.value+1
    grd[:,:,:,:], pdf[i] = cs.mpdf(
        [0,1,2], phot[i][detect[i]], photerr[i][detect[i]],
        filters=filt_to_use[i], ngrid=args.ngrid)

def phot_dist(i):
    if args.verbose:
        with ndone.get_lock():
            print "{:s}: getting photometric distances for cluster {:d}/{:d}".\
                format(current_process().name, nphotdone.value+1, ncl.value)
            nphotdone.value = nphotdone.value+1
    matches, dist[i] = cs.bestmatch(
        phot[i][detect[i]], filters=filt_to_use[i],
        nmatch=args.nneighbor)
    matches, distnorm[i] = cs.bestmatch(
        phot[i][detect[i]], photerr[i][detect[i]],
        filters=filt_to_use[i], nmatch=args.nneighbor)

# Prepare objects to hold results; these will be globally-shared arrays
if args.ndim == 1:
    proc_func = mpdf_1D
    logm_base = RawArray(ctypes.c_double, args.ngrid)
    logm = np.frombuffer(logm_base, dtype=ctypes.c_double)
    mpdf_base = RawArray(ctypes.c_double, phot.shape[0]*args.ngrid)
    mpdf = np.frombuffer(mpdf_base, dtype=ctypes.c_double). \
           reshape((phot.shape[0],args.ngrid))
    logt_base = RawArray(ctypes.c_double, args.ngrid)
    logt = np.frombuffer(logt_base, dtype=ctypes.c_double)
    tpdf_base = RawArray(ctypes.c_double, phot.shape[0]*args.ngrid)
    tpdf = np.frombuffer(tpdf_base, dtype=ctypes.c_double). \
           reshape((phot.shape[0],args.ngrid))
    AV_base = RawArray(ctypes.c_double, args.ngrid)
    AV = np.frombuffer(AV_base, dtype=ctypes.c_double)
    AVpdf_base = RawArray(ctypes.c_double, phot.shape[0]*args.ngrid)
    AVpdf = np.frombuffer(AVpdf_base, dtype=ctypes.c_double). \
            reshape((phot.shape[0],args.ngrid))
elif args.ndim == 2:
    proc_func = mpdf_2D
    grd_base = RawArray(ctypes.c_double, 2*args.ngrid**2)
    grd = np.frombuffer(grd_base, dtype=ctypes.c_double). \
          reshape((2,args.ngrid,args.ngrid))
    pdf_base = RawArray(ctypes.c_double, phot.shape[0]*args.ngrid**2)
    pdf = np.frombuffer(pdf_base, dtype=ctypes.c_double). \
          reshape((phot.shape[0],args.ngrid,args.ngrid))
elif args.ndim == 3:
    proc_func = mpdf_3D
    grd_base = RawArray(ctypes.c_double, 3*args.ngrid**3)
    grd = np.frombuffer(grd_base, dtype=ctypes.c_double). \
          reshape((3,args.ngrid,args.ngrid,args.ngrid))
    pdf_base = RawArray(ctypes.c_double, phot.shape[0]*args.ngrid**3)
    pdf = np.frombuffer(pdf_base, dtype=ctypes.c_double). \
          reshape((phot.shape[0],args.ngrid,args.ngrid,args.ngrid))
else:
    raise ValueError("ndim must be 1, 2, or 3")
if args.nneighbor > 0:
    dist_base = RawArray(ctypes.c_double, phot.shape[0]*args.nneighbor)
    dist = np.frombuffer(dist_base, dtype=ctypes.c_double). \
           reshape((phot.shape[0], args.nneighbor))
    distnorm_base = RawArray(ctypes.c_double, 
                             phot.shape[0]*args.nneighbor)
    distnorm = np.frombuffer(distnorm_base, dtype=ctypes.c_double). \
               reshape((phot.shape[0], args.nneighbor))

# Set up work counters to use in printing status information
ncl = Value('i', 0)
ndone = Value('i', 0)
nphotdone = Value('i', 0)

# Start the master thread and the pool of workers
if __name__ == '__main__':

    # Create the parallel process pool
    if args.nproc <= 0:
        print "Starting {:d} processes".format(cpu_count())
        pool = Pool()
    else:
        print "Starting {:d} processes".format(args.nproc)
        pool = Pool(processes=args.nproc)

    # Compute posterior PDFs
    if args.verbose:
        print("Computing posterior PDFs...")
    ncl.value = len(phot)
    pool.map(proc_func, range(ncl.value))

    # Compute photometric distances
    if args.verbose:
        print("Computing photometric distances for cases with all filters...")
    pool.map(phot_dist, range(ncl.value))
    dist = np.transpose(
        np.transpose(dist)/np.sqrt(np.sum(detect, axis=1)))
    distnorm = np.transpose(
        np.transpose(distnorm)/np.sqrt(np.sum(detect, axis=1)))

    # Close the pool
    pool.close()
    pool.join()

    # Construct output filename
    if args.outfile is None:
        if args.ndim == 1:
            outfile = args.photfile + '_pdf_1D.fits'
        elif args.ndim == 2:
            outfile = args.photfile + '_pdf_2D.fits'
        else:
            outfile = args.photfile + '_pdf_3D.fits'
    else:
        outfile = args.outfile + '.fits'

    # Write outputs to file
    if args.verbose:
        print("Writing output to {}...".format(outfile))
    cols1 = []
    if args.ndim == 2:
        logm = np.copy(grd[0,:,0])
        logt = np.copy(grd[1,0,:])
    elif args.ndim == 3:
        logm = np.copy(grd[0,:,0,0])
        logt = np.copy(grd[1,0,:,0])
        AV = np.copy(grd[2,0,0,:])
    cols1.append(fits.Column(name='log_m', format='1D',
                             unit='Msun', array=logm))
    cols1.append(fits.Column(name='log_t', format='1D',
                             unit='yr', array=logt))
    if args.ndim != 2:
        cols1.append(fits.Column(name='AV', format='1D',
                                 unit='yr', array=AV))
    cols1fits = fits.ColDefs(cols1)
    cols1hdu = fits.BinTableHDU.from_columns(cols1fits)
    cols2 = []
    cols2.append(fits.Column(name='Cluster_ID', format='1J',
                             array=cid))
    if args.ndim == 1:
        pdffmt = str(mpdf.shape[1])+'D'
        cols2.append(fits.Column(name='Mass_PDF', format=pdffmt,
                                 unit='', array=mpdf))
        cols2.append(fits.Column(name='Age_PDF', format=pdffmt,
                                 unit='', array=tpdf))
        cols2.append(fits.Column(name='AV_PDF', format=pdffmt,
                                 unit='', array=AVpdf))
    else:
        pdffmt = str(pdf.size/pdf.shape[0])+'D'
        cols2.append(fits.Column(
            name='PDF', format=pdffmt, unit='',
            array=pdf.reshape((pdf.shape[0], pdf.size/pdf.shape[0]))))
    if args.nneighbor > 0:
        cols2.append(fits.Column(name='Phot_dist', 
                                 format='{:d}D'.format(args.nneighbor),
                                 unit='mag', array=dist))
        cols2.append(fits.Column(name='Phot_dist_norm', 
                                 format='{:d}D'.format(args.nneighbor),
                                 array=distnorm))
    cols2_fits = fits.ColDefs(cols2)
    cols2_hdu = fits.BinTableHDU.from_columns(cols2_fits)
    prihdu = fits.PrimaryHDU()
    hdulist = fits.HDUList([prihdu, cols1hdu, cols2_hdu])
    hdulist.writeto(outfile, clobber=True)

