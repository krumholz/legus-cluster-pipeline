# This script reads the posterior probability files and writes them
# out in the form of machine-readable ASCII tables conforming to the
# CDS standard. It also prints out a stub of the table in latex
# format, and writes out the full analysis in FITS format.

from astropy.io import fits
from astropy.io import ascii as apascii
import os
import os.path as osp
import numpy as np

# Input photometry files
photfiles = ['ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab',
             'ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab'
             ]

# Index of the nuclear cluster, so we can move it to the low
# confidence catalog
nuc_cl = [-1, 321, 822]

# Location of the posterior PDF files, and list of galaxies
#fitdir = '/Users/krumholz/Data/legus/fits/'
fitdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1/'
galnames = ['ngc628e', 'ngc7793e', 'ngc7793w']
gallabels = ['NGC\,628', 'NGC\,7793e', 'NGC\,7793w']
outnames = ['ngc0628', 'ngc7793e', 'ngc7793w']
models = ['modp020_kroupalim_MW_phi0.50', 'modp020_kroupalim_SB_phi0.50',
          'modp004_kroupalim_MW_phi0.50', 'modp008_kroupalim_MW_phi0.50',
          'modp020_chabrier_MW_phi0.50', 'Z0140v00_kroupalim_MW_phi0.50',
          'modp020_kroupalim_MW_phi0.50_noneb']
modnames = ['pad_020_kroupa_MW', 'pad_020_kroupa_SB',
            'pad_004_kroupa_MW', 'pad_008_kroupa_MW',
            'pad_020_chabrier_MW', 'gen_014_kroupa_MW',
            'pad_020_kroupa_MW_noneb']
modlatex = ['pad\\_020\\_kroupa\\_MW', 'pad\\_020\\_kroupa\\_SB',
            'pad\\_004\\_kroupa\\_MW', 'pad\\_008\\_kroupa\\_MW',
            'pad\\_020\\_chabrier\\_MW', 'gen\\_014\\_kroupa\\_MW',
            'pad\\_020\\_kroupa\\_MW\\_noneb']

# List of which sets of priors to include
msprior = [2]
tsprior = [0, 0.5, 1]

# Read the data we need from the photometry files
phot = []
for pf in photfiles:
    data = apascii.read(pf)
    cid = data['col1']
    modeclass = data['col34']
    meanclass = data['col35']
    phot.append({'cid' : cid, 'modeclass' : modeclass, 
                 'meanclass' : meanclass})

# Split the photometry data into high confidence and low confidence
# subsets
for nidx, ph in zip(nuc_cl, phot):
    highidx = np.logical_and(ph['modeclass'] > 0, ph['modeclass'] < 3.5)
    lowidx = np.logical_or(ph['modeclass'] <= 0, ph['modeclass'] >=
                           3.5)
    if nidx in ph['cid']:
        highidx[list(ph['cid']).index(nidx)] = False
        lowidx[list(ph['cid']).index(nidx)] = True
    ph['cid_high'] = ph['cid'][highidx]
    ph['modeclass_high'] = ph['modeclass'][highidx]
    ph['meanclass_high'] = ph['meanclass'][highidx]
    ph['cid_low'] = ph['cid'][lowidx]
    ph['modeclass_low'] = ph['modeclass'][lowidx]
    ph['meanclass_low'] = ph['meanclass'][lowidx]
    ph['highidx'] = highidx
    ph['lowidx'] = lowidx

# Read the posterior PDFs, and compute their percentiles
csfit = []
for ph, gal in zip(phot, galnames):
    csfitgal = []
    for mod, modname, modlat in zip(models, modnames, modlatex):
        for mpr in msprior:
            for tpr in tsprior:
                fname = osp.join(fitdir,
                                 gal+'_'+mod+'_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.
                                 format(float(mpr), float(tpr)))
                hdulist = fits.open(fname)
                logm = hdulist[1].data['log_m']
                logt = hdulist[1].data['log_t']
                AV = hdulist[1].data['AV']
                cid = hdulist[2].data['Cluster_ID']
                mpdf = hdulist[2].data['Mass_PDF']
                tpdf = hdulist[2].data['Age_PDF']
                AVpdf = hdulist[2].data['AV_PDF']
                dist = hdulist[2].data['Phot_dist']
                dist_norm = hdulist[2].data['Phot_dist_norm']
                hdulist.close()

                # Compute percentiles
                mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
                logm_pct = np.array([
                    logm[np.argmax(np.greater(mpdfsum, 0.16), axis=1)],
                    logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
                    logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
                    logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
                    logm[np.argmax(np.greater(mpdfsum, 0.84), axis=1)]])
                tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
                logt_pct = np.array([
                    logt[np.argmax(np.greater(tpdfsum, 0.16), axis=1)],
                    logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
                    logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
                    logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
                    logt[np.argmax(np.greater(tpdfsum, 0.84), axis=1)]])
                AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
                AV_pct = np.array([
                    AV[np.argmax(np.greater(AVpdfsum, 0.16), axis=1)],
                    AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
                    AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
                    AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
                    AV[np.argmax(np.greater(AVpdfsum, 0.84),
                                 axis=1)]])

                # Split into high and low confidence samples
                mpdf_high = mpdf[ph['highidx']]
                tpdf_high = tpdf[ph['highidx']]
                AVpdf_high = AVpdf[ph['highidx']]
                logm_pct_high = logm_pct[:,ph['highidx']]
                logt_pct_high = logt_pct[:,ph['highidx']]
                AV_pct_high = AV_pct[:,ph['highidx']]
                dist_high = dist[ph['highidx']]
                dist_norm_high = dist_norm[ph['highidx']]
                cid_high = cid[ph['highidx']]
                mpdf_low = mpdf[ph['lowidx']]
                tpdf_low = tpdf[ph['lowidx']]
                AVpdf_low = AVpdf[ph['lowidx']]
                logm_pct_low = logm_pct[:,ph['lowidx']]
                logt_pct_low = logt_pct[:,ph['lowidx']]
                AV_pct_low = AV_pct[:,ph['lowidx']]
                dist_low = dist[ph['lowidx']]
                dist_norm_low = dist_norm[ph['lowidx']]
                cid_low = cid[ph['lowidx']]

                # Save data
                csfitgal.append(
                    { 'ms' : float(mpr), 'ts' : float(tpr),
                      'model' : modname, 'modlatex' : modlat,
                      'logm' : logm, 'logt' : logt, 'AV' : AV, 
                      'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
                      'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
                      'AV_pct' : AV_pct, 'dist' : dist, 'dist_norm' :
                      dist_norm, 'id' : cid,
                      'mpdf_high' : mpdf_high, 
                      'tpdf_high' : tpdf_high, 
                      'AVpdf_high' : AVpdf_high,
                      'logm_pct_high' : logm_pct_high, 
                      'logt_pct_high' : logt_pct_high,
                      'AV_pct_high' : AV_pct_high, 
                      'dist_high' : dist_high,
                      'dist_norm_high' : dist_norm_high,
                      'id_high' : cid_high,
                      'mpdf_low' : mpdf_low, 
                      'tpdf_low' : tpdf_low, 
                      'AVpdf_low' : AVpdf_low,
                      'logm_pct_low' : logm_pct_low, 
                      'logt_pct_low' : logt_pct_low,
                      'AV_pct_low' : AV_pct_low, 
                      'dist_low' : dist_low,
                      'dist_norm_low' : dist_norm_low,
                      'id_low' : cid_low
                  })

    # Save
    csfit.append(csfitgal)

# Write out the readme for the MRT
try:
    os.mkdir('mrt')
except:
    pass
fp = open(osp.join('mrt', 'ReadMe'), 'w')
fp.write('XXXX:    Derived Star Cluster Properties in NGC0628 and NGC7793 (Krumholz+ 2015)\n')
fp.write('================================================================================\n')
fp.write('Star Cluster Properties in LEGUS Galaxies Derived with Stochastic Stellar \n     Population Synthesis Models\n')
fp.write('     Krumholz, M.R., Adamo, A., Fumagalli, M., Wofford, A., Calzetti, D., \n     Lee, J.C.\n')
fp.write('    <JOURNAL REF HERE>\n')
fp.write('================================================================================\n')
fp.write('ADC_Keywords: Photometry:wide-band; Open_Clusters\n')
fp.write('\n')
fp.write('Description:\n')
fp.write('    This catalog contains summary posterior probability distribution functions\n')
fp.write('    for the masses, ages, and extinctions of star clusters in NGC0628 and\n')
fp.write('    NGC7793 derived by processing the LEGUS photometric catalog through the\n')
fp.write('    stochastic stellar population synthesis fitting tool cluster_slug (Krumholz+,\n')
fp.write('    2015, MNRAS, 452, 1447). For each cluster this catalog contains the\n')
fp.write('    16th, 25th, 50th, 75th, and 84th percentile values for its marginal posterior\n')
fp.write('    PDF of mass, age, and extinction, derived with a range of model libraries.\n')
fp.write('\n\n')
fp.write('File Summary:\n')
fp.write('--------------------------------------------------------------------------------\n')
fp.write(' FileName    Lrecl    Records    Explanations\n')
fp.write('--------------------------------------------------------------------------------\n')
fp.write('ReadMe          80          .    This file\n')
fp.write('ngc0628.dat    158       {:4d}    NGC0628 high confidence clusters\n'.format(len(phot[0]['cid_high'])))
fp.write('ngc7793e.dat   158       {:4d}    NGC7793 East high confidence clusters\n'.format(len(phot[1]['cid_high'])))
fp.write('ngc7793w.dat   158       {:4d}    NGC7793 West high confidence clusters\n'.format(len(phot[2]['cid_high'])))
fp.write('ngc0628_l.dat  158       {:4d}    NGC0628 low confidence clusters\n'.format(len(phot[0]['cid_low'])))
fp.write('ngc7793e_l.dat 158       {:4d}    NGC7793 East low confidence clusters\n'.format(len(phot[1]['cid_low'])))
fp.write('ngc7793w_l.dat 158       {:4d}    NGC7793 West low confidence clusters\n'.format(len(phot[2]['cid_low'])))
fp.write('--------------------------------------------------------------------------------\n\n')
fp.write('Byte-by-byte Description of file: ngc0628.dat ngc7793e.dat ngc7793w.dat ngc0628_l.dat ngc7793e_l.dat ngc7793w_l.dat\n')
fp.write('--------------------------------------------------------------------------------\n')
fp.write('   Bytes Format  Units   Label    Explanations\n')
fp.write('--------------------------------------------------------------------------------\n')
fp.write('   1-  3  I3     ---     ID       ID in the LEGUS photometric catalog\n')
fp.write('   5-  8  F4.2   ---   ClassMode  Visual classification, mode of classifiers\n')
fp.write('  10- 13  F4.2   ---   ClassMean  Visual classification, mean of classifiers\n')
fp.write('  14- 37  A24    ---     Lib      Name of the cluster_slug library\n')
fp.write('  39- 42  F4.1   ---     beta     Slope of the mass prior, dN/dM ~ M^beta\n')
fp.write('  44- 47  F4.1   ---     gamma    Slope of the age prior, dN/dT ~ T^gamma\n')
fp.write('  49- 54  F6.3   Msun    logM_14  14th percentile of log mass\n')
fp.write('  55- 61  F6.3   Msun    logM_25  25th percentile of log mass\n')
fp.write('  63- 68  F6.3   Msun    logM_50  50th percentile of log mass\n')
fp.write('  70- 75  F6.3   Msun    logM_75  75th percentile of log mass\n')
fp.write('  77- 82  F6.3   Msun    logM_84  84th percentile of log mass\n')
fp.write('  84- 89  F6.3   yr      logT_14  14th percentile of log age\n')
fp.write('  91- 96  F6.3   yr      logT_25  25th percentile of log age\n')
fp.write('  98-103  F6.3   yr      logT_50  50th percentile of log age\n')
fp.write(' 105-110  F6.3   yr      logT_75  75th percentile of log age\n')
fp.write(' 112-117  F6.3   yr      logT_84  84th percentile of log age\n')
fp.write(' 119-123  F5.3   mag     AV_14    14th percentile of A_V\n')
fp.write(' 125-129  F5.3   mag     AV_25    25th percentile of A_V\n')
fp.write(' 131-135  F5.3   mag     AV_50    50th percentile of A_V\n')
fp.write(' 137-141  F5.3   mag     AV_75    75hh percentile of A_V\n')
fp.write(' 143-147  F5.3   mag     AV_84    84th percentile of A_V\n')
fp.write(' 149-155  F7.3   mag     D_5      5th nearest neighbor distance in library\n')
fp.write(' 152-158  F7.3   ---     Dnorm_5  5th nearest neighbor distance in library,\n')
fp.write('                                      normalized to photometric errors\n')
fp.write('--------------------------------------------------------------------------------\n')
fp.write('Note on classifications: 0 = source was not visually classified (too faint);\n')
fp.write('    1 = symmetric, compact cluster; 2 = concentrated object with some degree\n')
fp.write('    of asymmetry or color gradient; 3 = diffuse or multiple peak system,\n')
fp.write('    possibly spurious alignment; 4 = spurious detection (foreground/background\n')
fp.write('    source, single bright star, artifact)\n')
fp.write('Note on cluster_slug libraries: name format is NNN_ZZZ_III_EE, where NNN is\n')
fp.write('    pad for Padova tracks, gen for Geneva tracks; ZZZ indicates metallicity\n')
fp.write('    (020 = 0.02, 004 = 0.04, etc.), III = kroupa for Kroupa IMF, chabrier for\n')
fp.write('    chabrier IMF, EE = MW for Milky Way extinction law, SB for starburst\n')
fp.write('    extinction law\n')
fp.write('--------------------------------------------------------------------------------\n\n')
fp.write("Author's address:\n")
fp.write('    Mark Krumholz    <mkrumhol@ucsc.edu>\n\n')
fp.write('================================================================================\n')
fp.write('(End)             Mark Krumholz [UC Santa Cruz]                       7-Apr-2015')
fp.close()


# Now write out the MRTs
for ph, gal, csgal in zip(phot, outnames, csfit):

    # Open file
    fp = open(osp.join('mrt', gal+'.dat'), 'w')

    # Loop over models and priors
    for cs in csgal:
        for i in range(len(cs['id_high'])):
            fp.write(("{:3d} {:4.2f} {:4.2f} {:24s} {:4.1f} {:4.1f} {:6.3f} "+
                      "{:6.3f} {:6.3f} {:6.3f} {:6.3f} {:6.3f} {:6.3f} "+
                      "{:6.3f} {:6.3f} {:6.3f} {:5.3f} {:5.3f} {:5.3f} "+
                      "{:5.3f} {:5.3f} {:7.3f} {:7.3f}\n").format(
                          cs['id_high'][i], ph['modeclass_high'][i],
                          ph['meanclass_high'][i],
                          cs['model'], -cs['ms'], -cs['ts'],
                          cs['logm_pct_high'][0,i], 
                          cs['logm_pct_high'][1,i],
                          cs['logm_pct_high'][2,i], 
                          cs['logm_pct_high'][3,i],
                          cs['logm_pct_high'][4,i],
                          cs['logt_pct_high'][0,i],
                          cs['logt_pct_high'][1,i],
                          cs['logt_pct_high'][2,i],
                          cs['logt_pct_high'][3,i],
                          cs['logt_pct_high'][4,i],
                          cs['AV_pct_high'][0,i], 
                          cs['AV_pct_high'][1,i],
                          cs['AV_pct_high'][2,i], 
                          cs['AV_pct_high'][3,i],
                          cs['AV_pct_high'][4,i],
                          cs['dist_high'][i,4],
                          cs['dist_norm_high'][i,4]))

    # Close the file
    fp.close()

    # Open file for low confidence lusters
    fp = open(osp.join('mrt', gal+'_l.dat'), 'w')

    # Loop over models and priors
    for cs in csgal:
        for i in range(len(cs['id_low'])):
            fp.write(("{:3d} {:4.2f} {:4.2f} {:24s} {:4.1f} {:4.1f} {:6.3f} "+
                      "{:6.3f} {:6.3f} {:6.3f} {:6.3f} {:6.3f} {:6.3f} "+
                      "{:6.3f} {:6.3f} {:6.3f} {:5.3f} {:5.3f} {:5.3f} "+
                      "{:5.3f} {:5.3f} {:7.3f} {:7.3f}\n").format(
                          cs['id_low'][i], ph['modeclass_low'][i],
                          ph['meanclass_low'][i],
                          cs['model'], -cs['ms'], -cs['ts'],
                          cs['logm_pct_low'][0,i], 
                          cs['logm_pct_low'][1,i],
                          cs['logm_pct_low'][2,i], 
                          cs['logm_pct_low'][3,i],
                          cs['logm_pct_low'][4,i],
                          cs['logt_pct_low'][0,i],
                          cs['logt_pct_low'][1,i],
                          cs['logt_pct_low'][2,i],
                          cs['logt_pct_low'][3,i],
                          cs['logt_pct_low'][4,i],
                          cs['AV_pct_low'][0,i], 
                          cs['AV_pct_low'][1,i],
                          cs['AV_pct_low'][2,i], 
                          cs['AV_pct_low'][3,i],
                          cs['AV_pct_low'][4,i],
                          cs['dist_low'][i,4],
                          cs['dist_norm_low'][i,4]))

    # Close the file
    fp.close()

# Write the header for the latex stub
fp=open(osp.join('mrt', 'cluster_tab.tex'), 'w')
fp.write('\\begin{turnpage}\n')
fp.write('\\begin{deluxetable*}{lccccccccccccccccccccccc}\n')
#fp.write('\\rotate\n')
fp.write('\\tabletypesize{\\scriptsize}\n')
fp.write('\\tablecaption{Summary marginal posterior PDFs\label{tab:summary}}\n')
fp.write('\\tablehead{\n')
fp.write('\\colhead{ID} & \\colhead{Mode class\\tablenotemark{a}} & \\colhead{Mean class\\tablenotemark{a}} & \\colhead{} & \n')
fp.write('\\multicolumn{5}{c}{$\\log (M/M_\\odot)$ percentiles} & \\colhead{} &\n')
fp.write('\\multicolumn{5}{c}{$\\log (T/\\mathrm{yr})$ percentiles} & \\colhead{} &\n')
fp.write('\\multicolumn{5}{c}{$A_V$ percentiles} & \\colhead{} & \n')
fp.write('\\colhead{$D_5$ [mag]} & \\colhead{$D_5^{\\mathrm{norm}}$} \\\\[0.5ex]\n')
fp.write('\\cline{5-9} \\cline{11-15} \\cline{17-21}\\\\[-0.5ex]\n')
fp.write('\\colhead{} & \\colhead{} & \\colhead{} & \\colhead{} &\n')
fp.write('\\colhead{16} & \\colhead{25} & \\colhead{50} & \\colhead{75} & \\colhead{84} & \\colhead{} &\n')
fp.write('\\colhead{16} & \\colhead{25} & \\colhead{50} & \\colhead{75} & \\colhead{84} & \\colhead{} &\n')
fp.write('\\colhead{16} & \\colhead{25} & \\colhead{50} & \\colhead{75} & \\colhead{84} & \\colhead{} &\n')
fp.write('\\colhead{} & \\colhead{} \n')
fp.write('}\n')
fp.write('\\startdata\n')
for ph, gal, csgal in zip(phot[:2], gallabels[:2], csfit[:2]):
    fp.write('\\cutinhead{'+gal+'}\n')
    for j, cs in enumerate(csgal):
        fp.write('\\sidehead{'+cs['modlatex']+
                 ', $\\beta={:4.1f}$'.format(-cs['ms']) +
                 ', $\\gamma={:4.1f}$'.format(-cs['ts']) +
                 '}\n')
        for i in range(3):
            fp.write(('{:d} & {:4.2f} & {:4.2f} & & ' +
                      '{:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & &'+
                      '{:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & &'+
                      '{:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & {:4.2f} & &').
                      format(cs['id_high'][i], 
                             ph['modeclass_high'][i],
                             ph['meanclass_high'][i], 
                             cs['logm_pct_high'][0,i], 
                             cs['logm_pct_high'][1,i],
                             cs['logm_pct_high'][2,i],
                             cs['logm_pct_high'][3,i],
                             cs['logm_pct_high'][4,i],
                             cs['logt_pct_high'][0,i],
                             cs['logt_pct_high'][1,i],
                             cs['logt_pct_high'][2,i], 
                             cs['logt_pct_high'][3,i],
                             cs['logt_pct_high'][4,i],
                             cs['AV_pct_high'][0,i], 
                             cs['AV_pct_high'][1,i],
                             cs['AV_pct_high'][2,i],
                             cs['AV_pct_high'][3,i],
                             cs['AV_pct_high'][4,i]))
            if cs['dist_norm_high'][i,4] < 10:
                fp.write('{:4.2f} & \\phn{:3.2f} \\\\\n'.
                         format(cs['dist_high'][i,4], 
                                cs['dist_norm_high'][i,4]))
            else:
                fp.write('{:4.2f} & {:4.2f} \\\\\n'.
                         format(cs['dist_high'][i,4], 
                                cs['dist_norm_high'][i,4]))
        if j == 3:
            break
fp.write('\\enddata\n')
fp.write('\\tablenotetext{a}{Mode and mean of the classifications given by the visual classifiers; 0 = source was not visually classified (too faint); 1 = symmetric, compact cluster; 2 = concentrated object with some degree of asymmetry or color gradient; 3 = diffuse or multiple peak system, possibly spurious alignment; 4 = probable spurious detection (foreground/background source, single bright star, artifact}\n')
fp.write('\\tablecomments{\\autoref{tab:summary} appears in its entirety in the electronic edition of The Astrophysical Journal. A portion is shown here for guidance regarding its form and content. \\red{The full table includes'+
         '{:d}'.format(len(phot[0]['cid_high']) + len(phot[1]['cid_high']) +
                       len(phot[2]['cid_high'])) + 
         ' visually-confirmed clusters, and '+
         '{:d}'.format(len(phot[0]['cid_low']) + len(phot[1]['cid_low']) +
                       len(phot[2]['cid_low'])) +
         ' objects visually-classified as unlikely to be clusters (provided in separate files).} }\n')
fp.write('\\end{deluxetable*}\n')
fp.write('\\end{turnpage}\n')

fp.close()
