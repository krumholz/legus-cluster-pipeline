# This script plots the distribution of nearest neighbor distances
# between the cluster_slug libraries and the observed catalogs.

import numpy as np
import matplotlib.pyplot as plt
import glob
import os.path as osp
from astropy.io import ascii as apyascii
from astropy.io import fits as apyfits
from slugpy import read_cluster

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
galname = ['NGC7793e', 'NGC7793w', 'NGC0628e']

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Names of slug libraries
csfiles = ['/Users/krumholz/Data/legus/libs/modp020_kroupalim_MW_phi0.50'
           , '/Users/krumholz/Data/legus/libs/modp020_kroupalim_SB_phi0.50'
           , '/Users/krumholz/Data/legus/libs/modp004_kroupalim_MW_phi0.50'
           , '/Users/krumholz/Data/legus/libs/modp008_kroupalim_MW_phi0.50'
           , '/Users/krumholz/Data/legus/libs/modp020_chabrier_MW_phi0.50'
           , '/Users/krumholz/Data/legus/libs/Z0140v00_kroupalim_MW_phi0.50'
           , '/Users/krumholz/Data/legus/libs/modp020_kroupalim_MW_phi0.50_noneb'
       ]
libdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1'
libname = ['pad_020_kroupa_MW'
           , 'pad_020_kroupa_SB'
           , 'pad_004_kroupa_MW'
           , 'pad_008_kroupa_MW'
           , 'pad_020_chabrier_MW'
           , 'gen_014_kroupa_MW'
           , 'pad_020_kroupa_\nMW_noneb'
       ]

# List of filters
filters = [ ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
             'ACS_F555W', 'ACS_F814W'],
            ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
             'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W'],
            ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'ACS_F435W',
             'WFC3_UVIS_F555W', 'ACS_F814W'] ]

# Read the photometry files
cid = []
classidx = []
nidx = 0
for pf, nucidx in zip(photfiles, nuc_cl):

    # Read data
    data = apyascii.read(pf)

    # Grab indices of high confidence clusters
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    classification = classification[classidx[-1]]

    # Grab IDs, RA, and DEC
    cid.append(data['col1'])

    # Set the nuclear cluster to false
    idlist = list(cid[-1])
    if nucidx in idlist:
        classidx[-1][idlist.index(nucidx)] = False

    # Count clusters
    nidx = nidx + np.sum(classidx[-1])

# Read nearest neighbor distances from analysis files
csdist = []
csdistnorm = []
csnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
for i in range(len(csfiles)):
    csdist.append([])
    csdistnorm.append([])
    for j in range(len(csnames)):
        fname = osp.join(libdir, 
                         csnames[j]+'_'+osp.basename(csfiles[i])+
                         '_ms2.0_ts0.0_pdf_1D.fits')
        hdulist = apyfits.open(fname)
        csdist[i].append(hdulist[2].data['Phot_dist'][classidx[j]])
        csdistnorm[i].append(hdulist[2].data['Phot_dist_norm'][classidx[j]])
        hdulist.close()

# Plot cumulative distance distribution
fig = plt.figure(1, figsize=(5,10))
colors = ['b', 'g', 'r']
linestyles = ['-', '--', '-.']
plt.clf()

# Make common plot label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel('Cumulative fraction', labelpad=20)

d5all = np.zeros(0)
d5mod = []
d5allnorm = np.zeros(0)
d5modnorm = []
for i in range(len(csdist)):

    # Plot photometric distance distribution
    ax = fig.add_subplot(len(csdist),2,2*i+1)
    pl = []
    d5modtmp = np.zeros(0)
    for j in range(len(csdist[i])):
        d5 = csdist[i][j][:,4]
        d5.sort()
        p1,=plt.plot(d5, np.linspace(0, 1, len(d5)),
                     colors[j]+linestyles[j], lw=2)
        pl.append(p1)
        d5all = np.append(d5all, d5)
        d5modtmp = np.append(d5modtmp, d5)
    d5modtmp.sort()
    d5mod.append(d5modtmp)
    p1,=plt.plot(d5mod[-1], np.linspace(0, 1, len(d5mod[-1])),
                 'k', lw=2)
    pl.append(p1)
    plt.plot([0.1, 0.1], [0, 1], 'k--')
    if i != len(csdist)-1:
        ax.set_xticklabels([])
    else:
        plt.xlabel(r'$D_5$ [mag]', labelpad=12)
    plt.xlim([0,0.5])
    ax.set_xticks([0, 0.1, 0.2, 0.3, 0.4])
    if i != 0:
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8])
    else:
        ax.set_yticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    plt.text(0.475, 0.075, libname[i], bbox={'facecolor':'white'},
             horizontalalignment='right', size=10)

    # Plot normalized photometric distance distribution
    ax = fig.add_subplot(len(csdist),2,2*i+2)
    pl = []
    d5modtmpnorm = np.zeros(0)
    for j in range(len(csdist[i])):
        d5norm = csdistnorm[i][j][:,4]
        d5norm.sort()
        p1,=plt.plot(d5norm, np.linspace(0, 1, len(d5norm)),
                     colors[j]+linestyles[j], lw=2)
        pl.append(p1)
        d5allnorm = np.append(d5allnorm, d5norm)
        d5modtmpnorm = np.append(d5modtmpnorm, d5norm)
    d5modtmpnorm.sort()
    d5modnorm.append(d5modtmpnorm)
    p1,=plt.plot(d5modnorm[-1], np.linspace(0, 1, len(d5modnorm[-1])),
                 'k', lw=2)
    pl.insert(0, p1)
    if i != len(csdist)-1:
        ax.set_xticklabels([])
    else:
        plt.xlabel(r'$D_5^{\mathrm{norm}}$', labelpad=12)
    ax.set_yticklabels([])
    plt.xlim([0,4])
    plt.xticks([0,1,2,3,4])
    if i == 0:
        plt.legend(pl, ['All']+galname, loc='lower right', prop={'size':10})

    # Print some information
    print("Model "+libname[i] + 
          ": CDF(D5norm) = " +
          "{:5.3f} at 1 sigma, {:5.3f} at 2 sigma, {:5.3f} at 3 sigma".
          format(
              np.argmax(d5modnorm[i] > 1)/float(len(d5modnorm[i])),
              np.argmax(d5modnorm[i] > 2)/float(len(d5modnorm[i])),
              np.argmax(d5modnorm[i] > 3)/float(len(d5modnorm[i]))
          ))

plt.subplots_adjust(hspace=0, wspace=0, bottom=0.06, top=0.97)
plt.savefig('error_dist.pdf')


# Plot various
fig=plt.figure(2, figsize=(6,4))
plt.clf()
ax=plt.subplot(1,1,1)
ax.plot(d5modnorm[0], np.linspace(0, 1, len(d5modnorm[0])),
        'k', lw=3, label='Measured')
x = np.arange(0, 4, 0.01)
l = 5*x
ax.plot(x, 1-np.exp(-l)-l*np.exp(-l)-l**2*np.exp(-l)/2-
        l**3*np.exp(-l)/6-l**4*np.exp(-l)/24,
        'b'+linestyles[0], label=r'$\ell=5$, $M=1$', lw=2)
l = 4*x
ax.plot(x, 1-np.exp(-l)-l*np.exp(-l)-l**2*np.exp(-l)/2-
        l**3*np.exp(-l)/6-l**4*np.exp(-l)/24,
        'g'+linestyles[1], label=r'$\ell=4$, $M=1$', lw=2)
l = 6*x
ax.plot(x, 1-np.exp(-l)-l*np.exp(-l)-l**2*np.exp(-l)/2-
        l**3*np.exp(-l)/6-l**4*np.exp(-l)/24,
        'r'+linestyles[2], label=r'$\ell=6$, $M=1$', lw=2)
l = 5*x**2
ax.plot(x, 1-np.exp(-l)-l*np.exp(-l)-l**2*np.exp(-l)/2-
        l**3*np.exp(-l)/6-l**4*np.exp(-l)/24,
        'm:', label=r'$\ell=5$, $M=2$', lw=2)
l = 5*x**3
ax.plot(x, 1-np.exp(-l)-l*np.exp(-l)-l**2*np.exp(-l)/2-
        l**3*np.exp(-l)/6-l**4*np.exp(-l)/24,
        'c.', label=r'$\ell=5$, $M=3$', lw=2)
ax.legend(loc='lower right')
ax.set_xlim([0,4])
ax.set_ylim([0,1])
ax.set_xlabel(r'$D_5^{\mathrm{norm}}$', labelpad=12)
ax.set_ylabel('Cumulative fraction')
plt.subplots_adjust(bottom=0.14)
plt.savefig('d5expectation.pdf')
