# This script reads the cluster_slug libraries and make a plot
# comparing the colors found therein to the observed colors.

import numpy as np
import matplotlib.pyplot as plt
import glob
import os.path as osp
from astropy.io import ascii as apyascii
from slugpy import read_cluster

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
galname = ['NGC7793e', 'NGC7793w', 'NGC0628e']

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Names of slug libraries
libdir = '/pfs/krumholz/cluster_slug/lib/'
csfiles = [osp.join(libdir, 'modp020_kroupalim_MW_phi0.50'),
           osp.join(libdir, 'modp020_kroupalim_SB_phi0.50'),
           osp.join(libdir, 'modp004_kroupalim_MW_phi0.50'),
           osp.join(libdir, 'modp008_kroupalim_MW_phi0.50'),
           osp.join(libdir, 'modp020_chabrier_MW_phi0.50'),
           osp.join(libdir, 'Z0140v00_kroupalim_MW_phi0.50'),
           osp.join(libdir, 'modp020_kroupalim_MW_phi0.50')
       ]
libname = ['pad_020_kroupa_MW',
           'pad_020_kroupa_SB',
           'pad_004_kroupa_MW',
           'pad_008_kroupa_MW',
           'pad_020_chabrier_MW',
           'gen_014_kroupa_MW',
           'pad_020_kroupa_MW_noneb']
nebular = [True, True, True, True, True, True, False]

# List of filters
filters=['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
         'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W']

# Read the photometry files
phot = []
photerr = []
photerrmean = []
detect = []
classidx = []
for pf, nucidx in zip(photfiles, nuc_cl):

    # Read data and grab indices of high confidence clusters
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    classification = classification[classidx[-1]]

    # Set the nuclear cluster to false
    idlist = list(data['col1'])
    if nucidx in idlist:
        classidx[-1][idlist.index(nucidx)] = False

    # Read photometry, flagging non-detections
    nidx = np.sum(classidx[-1])
    phot.append(np.zeros((nidx, 5)))
    photerr.append(np.zeros((nidx, 5)))
    detect.append(np.ones((nidx, 5), dtype=bool))
    for i in range(5):
        phot[-1][:,i] = data['col{:d}'.format(2*i+6)][classidx[-1]]
        photerr[-1][:,i] = data['col{:d}'.format(2*i+7)][classidx[-1]]
        detect[-1][:,i] = data['col{:d}'.format(2*i+6)][classidx[-1]] < 99.
    photerr[-1][np.logical_not(detect[-1])] = 0.0  # Null out non-detections
    photerrmean.append(np.sqrt(np.sum(photerr[-1]**2, axis=1)) /
                       np.sqrt(np.sum(photerr[-1] != 0.0, axis=1)))

# Read photometry from libraries
cslib = []
for cs, neb in zip(csfiles, nebular):
    cslib.append(
        read_cluster(cs, read_filters=filters,
                     read_nebular=neb, read_extinct=True,
                     nofilterdata=True))

# Plot color-color diagram
symbols = ['s', '^', 'o']
colors = ['b', 'g', 'r']
fig = plt.figure(1, figsize=(6,12))
plt.clf()

# Make common plot label
axc = fig.add_subplot(1,1,1)
axc.spines['top'].set_color('none')
axc.spines['bottom'].set_color('none')
axc.spines['left'].set_color('none')
axc.spines['right'].set_color('none')
axc.tick_params(top='off', bottom='off', left='off', right='off')
axc.set_xticklabels([])
axc.set_yticklabels([])
axc.set_ylabel('F336W - F275W', labelpad=20)

# Make scatter plots
int1 = 100
int2 = 20
ylim = [-3, 1.75]
cmap = 'RdYlBu_r'
ms = 4
marker = '.'
for i in range(len(cslib)):

    # F438 - F336 on x axis
    # F336 - F275 on y axis
    ax1 = fig.add_subplot(len(cslib),3,3*i+1)
    if nebular[i]:
        pl = cslib[i].phot_neb_ex
    else:
        pl = cslib[i].phot_ex
    ax1.scatter(pl[::int1,2] - pl[::int1,1],
                pl[::int1,1] - pl[::int1,0],
                c=np.log10(cslib[i].time[::int1]-
                           cslib[i].form_time[::int1]),
                vmin=5, vmax=10,
                marker=marker, linewidths=0,
                cmap=plt.get_cmap(cmap), edgecolor='',
                rasterized=True)
    pts = []
    for j in range(len(phot)):
        idx = np.sum(detect[j][::,:4], axis=1) == 4
        p1,=plt.plot((phot[j][idx,2] - phot[j][idx,1])[::int2], 
                     (phot[j][idx,1] - phot[j][idx,0])[::int2],
                     symbols[j], mfc=colors[j], ms=ms)
        pts.append(p1)
    plt.xlim([-3,3])
    plt.ylim(ylim)
    if i != len(cslib)-1:
        ax1.set_xticklabels([])
    ax1.set_xticks([-3, -2, -1, 0, 1, 2])
    if i != 0:
        ax1.set_yticks([-3, -2, -1, 0, 1])
    else:
        ax1.set_yticks([-3, -2, -1, 0, 1])
    if i == len(cslib)-1:
        ax1.set_xlabel('F438W - F336W')

    # F555 - F438 on x axis
    # F336 - F275 on y axis
    ax2 = fig.add_subplot(len(cslib),3,3*i+2)
    if nebular[i]:
        pl = cslib[i].phot_neb_ex
    else:
        pl = cslib[i].phot_ex
    ax2.scatter(pl[::int1,3] - pl[::int1,2],
                pl[::int1,1] - pl[::int1,0],
                c=np.log10(cslib[i].time[::int1]-
                           cslib[i].form_time[::int1]),
                vmin=5, vmax=10,
                marker=marker, linewidths=0,
                cmap=plt.get_cmap(cmap), edgecolor='',
                rasterized=True)
    for j in range(len(phot)):
        idx = np.sum(detect[j][::,:4], axis=1) == 4
        plt.plot((phot[j][idx,3] - phot[j][idx,2])[::int2], 
                 (phot[j][idx,1] - phot[j][idx,0])[::int2],
                 symbols[j], mfc=colors[j], label=galname[j], ms=ms)
    plt.xlim([-3,1])
    plt.ylim(ylim)
    if i != len(cslib)-1:
        ax2.set_xticklabels([])
    ax2.set_xticks([-3, -2, -1, 0])
    ax2.set_yticklabels([])
    if i == len(cslib)-1:
        ax2.set_xlabel('F555W - F438W')

    # F814 - F555 on x axis
    # F336 - F275 on y axis
    ax3 = fig.add_subplot(len(cslib),3,3*i+3)
    if nebular[i]:
        pl = cslib[i].phot_neb_ex
    else:
        pl = cslib[i].phot_ex
    plt.scatter(pl[::int1,4] - pl[::int1,3],
                pl[::int1,1] - pl[::int1,0],
                c=np.log10(cslib[i].time[::int1]-
                           cslib[i].form_time[::int1]),
                vmin=5, vmax=10,
                marker=marker, linewidths=0,
                cmap=plt.get_cmap(cmap), edgecolor='',
                rasterized=True)
    for j in range(len(phot)):
        idx = np.sum(detect[j][::,:4], axis=1) == 4
        plt.plot((phot[j][idx,4] - phot[j][idx,3])[::int2], 
                 (phot[j][idx,1] - phot[j][idx,0])[::int2],
                 symbols[j], mfc=colors[j], label=galname[j], ms=ms)
    plt.xlim([-6,2])
    plt.ylim(ylim)
    if i != len(cslib)-1:
        ax3.set_xticklabels([])
    ax3.set_xticks([-6, -4, -2, 0, 2])
    ax3.set_yticklabels([])
    if i == len(cslib)-1:
        ax3.set_xlabel('F814W - F555W')

    # Add label
    ax3.text(-10, 1.18, libname[i], bbox={'facecolor':'white'},
             horizontalalignment='center', size = 10)

# Put legend at the top
axc.legend(pts, galname, scatterpoints=1, numpoints=1, 
           prop={'size':10}, ncol=3, mode="expand",
           borderaxespad=0.,
           bbox_to_anchor=(0., 0.98, 1., 0.05))

# Adjust spacing
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.06, left=0.09, 
                    top=0.96, right=0.89)

# Add colorbar
axcbar = fig.add_axes((0.89, 0.06, 0.02, 0.9))
cbar = plt.colorbar(cax=axcbar)
cbar.set_label(r'$\log\,T$ [yr]')
cbar.set_ticks([5, 6, 7, 8, 9, 10])

# Save
plt.savefig('color_color.pdf')
