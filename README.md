### What's in this repo? ###

This repository contains scripts to process a set of photometry files
from [LEGUS](https://legus.stsci.edu/) through
[cluster_slug](http://www.slugsps.com) to produce marginal posterior
PDFs of mass, age, and extinction, and to make some plots from the
outputs of this comparison. The plotting scripts generate all the
figures in 
[Krumholz+ 2015, ApJ, 812, 147](http://adsabs.harvard.edu/abs/2015arXiv150905078K).

The following scripts are used first, to generate the posterior PDFs:

* *cspdf.py*: this is the main processing script; to run it just edit
  the top of the file to give the names and locations of the
  photometry files, the distance moduli to use, the names and
  locations of the cluster_slug libraries to use, and the parameters
  that describe the prior. Then do `python cspdf.py`.
* *getpdfs.py*: this is a subsidiary worker script called by
  cspdf.py. If computes the posterior PDFs on a single set of photometry
  with a single set of priors.

These require that you have [SLUG](http://www.slugsps.com) installed
and compiled, and that you have the LEGUS libraries downloaded from
the SLUG website.

The following scripts can be run once the posterior PDFs have been
generated in order to make some useful plots:

* *plot_colors.py*: this generates a color-color plot of the observed
  photometry in comparison to the model libraries; this appears as
  figure 1 in [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_errors.py*: this plots the distribution of photometric
  distances between models and data; its output appears as figures 2
  and 3 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_modelcomp.py*: this plots a comparison between the results
  derived with the fiducial model library and other libraries, which
  appears as figure 13 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_physprop.py*: this plots the PDFs of cluster physical
  properties from a sample of clusters; its output appears as figures
  4, 5, and 6 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_priorcompy.py*: this plots a comparison of the results for
  posterior probabilities derived using a variety of different priors;
  its outputs appear as figures 8, 9, 11, and 12 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_triangle.py*: this makes a triangle plot showing various 2D
  projections of the posterior PDFs for an example cluster; its output
  is figure 7 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_triangle_prior.py*: this makes triangle plots comparing
  results for two different prior distributions; its output is figure
  10 of [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)
* *plot_ygcomp.py*: this plots a comparison between the results of
  cluster_slug and yggdrasil; its outputs are figures 14 and 15 of
  [Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K)

The following scripts read the outputs from cspdf.py and process them
into the final forms that appear as a machine-readable table in
[Krumholz+ 2015](http://adsabs.harvard.edu/abs/2015arXiv150905078K),
and on the LEGUS website as FITS files:

* *write_fits_final.py*: this produces the FITS files that appear on
   the LEGUS website
* *write_mrt.py*: this produces a machine-readable table summary of
   the results

The following module contains a helpful utility routine:

* *read_PDFs.py*: this contains a function, read_PDFs, which reads the
   FITS file outputs containing the posterior PDFs as a series of
   numpy arrays. See the routine docstring for usage info.

### Questions, bugs ###

If you have questions about this code, or discover any bugs, please
contact [Mark
Krumholz](http://www.mso.anu.edu.au/~krumholz/),
mark.krumholz@anu.edu.au.

### Acknowledgements ###

Support for this work was provided by grants HST-GO-13364 and HST-AR-13256 from the Space Telescope Science Institute, which is operated by the Association of Universities for Research in Astronomy, Inc., under NASA contract NAS 5-26555.

### License ###

This code is distributed under the terms of the [GNU General Public License](http://www.gnu.org/copyleft/gpl.html) version 3.0. The text of the license is included in the main directory of the repository as GPL-3.0.txt.