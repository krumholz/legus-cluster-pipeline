# This script makes a plot that compares results with two different
# extinction curves

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii
import glob

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
distmod = [27.68, 27.72, 29.98]

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Specify cluster_slug models to use
fidlib = 'modp020_kroupalim_MW_phi0.50'
extlib = 'modp020_kroupalim_SB_phi0.50'
z1lib = 'modp004_kroupalim_MW_phi0.50'
z2lib = 'modp008_kroupalim_MW_phi0.50'
imflib = 'modp020_chabrier_MW_phi0.50'
tracklib = 'Z0140v00_kroupalim_MW_phi0.50'
neblib = 'modp020_kroupalim_MW_phi0.50_noneb'
alllibs = [fidlib, imflib, extlib, neblib, tracklib, z1lib, z2lib]
runlabels = ['Fiducial', 'Chabrier', 'SB ext', 'No nebular',
             'Geneva', r'$Z=0.004$', r'$Z=0.008$' ]
fitdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']
msfid = 2.0
tsfid = 0.5

# Go through photometry files to find high confidence clusters
classidx = []
for nidx, pf in zip(nuc_cl, photfiles):
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    cid = list(data['col1'])
    if nidx in cid:
        classidx[-1][cid.index(nidx)] = False

# Read the posterior marginal PDFs for the cases we're comparing
csfit = []
labels = []
for j in range(len(galnames)):

    # Loop over libraries
    csgal = []
    for i in range(len(alllibs)):

        # Construct the file name
        f = osp.join(fitdir, 
                     galnames[j]+'_'+alllibs[i]+
                     '_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.format(msfid, tsfid))

        # Read data
        try:
            hdulist = fits.open(f)
        except IOError:
            continue
        logm = hdulist[1].data['log_m']
        logt = hdulist[1].data['log_t']
        AV = hdulist[1].data['AV']
        mpdf = hdulist[2].data['Mass_PDF'][classidx[j]]
        tpdf = hdulist[2].data['Age_PDF'][classidx[j]]
        AVpdf = hdulist[2].data['AV_PDF'][classidx[j]]
        dist = hdulist[2].data['Phot_dist'][classidx[j]]
        hdulist.close()

        # Compute percentiles
        mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
        logm_pct = np.array([
            logm[np.argmax(np.greater(mpdfsum, 0.1), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.9), axis=1)]])
        tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
        logt_pct = np.array([
            logt[np.argmax(np.greater(tpdfsum, 0.1), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.9), axis=1)]])
        AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
        AV_pct = np.array([
            AV[np.argmax(np.greater(AVpdfsum, 0.1), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.9), axis=1)]])

        # Save data
        csgal.append(
            { 'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
              'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
              'AV_pct' : AV_pct, 'dist' : dist,
              'logm' : logm, 'logt' : logt, 'AV' : AV,
              'libname' : alllibs[i] })

        # Save label
        if j==0:
            labels.append(runlabels[i])

    # Save
    csfit.append(csgal)


# Plot comparisons
fig = plt.figure(1, figsize=(6,12))
plt.clf()
ngal = len(csfit)
nmod = len(csfit[0])
symbols = ['s', '^', 'o']
colors = ['b', 'g', 'r']
mra = [2, 6]
deltamra = [-4, 4]
tra = [5.5, 10.5]
deltatra = [-4, 4]
skip = 0.3

for i in range(1,nmod):

    # Make subplot for mass comparison
    ax = fig.add_subplot(nmod-1,2,2*i-1)

    # Loop over galaxies
    ptmp = []
    for j in range(ngal-1, -1, -1):

        # Grab data and sort it
        xdata = csfit[j][0]['logm_pct'][2,:]
        xerr = np.array([csfit[j][0]['logm_pct'][2,:]-
                         csfit[j][0]['logm_pct'][0,:],
                         csfit[j][0]['logm_pct'][4,:]-
                         csfit[j][0]['logm_pct'][2,:]])
        idxsrt = np.argsort(xdata)
        xdata = xdata[idxsrt]
        xerr = xerr[:,idxsrt]
        ydata = csfit[j][i]['logm_pct'][2,:]
        yerr = np.array([csfit[j][i]['logm_pct'][2,:]-
                         csfit[j][i]['logm_pct'][0,:],
                         csfit[j][i]['logm_pct'][4,:]-
                         csfit[j][i]['logm_pct'][2,:]])
        ydata = ydata[idxsrt]
        yerr = yerr[:,idxsrt]

        # Plot medians
        p, = ax.plot(xdata, ydata-xdata, colors[j]+symbols[j])
        ptmp.append(p)

        # Plot error bars on some points
        idx = [0]
        for l in range(len(xdata)):
            if xdata[l] - xdata[idx[-1]] > skip:
                idx.append(l)
        ax.errorbar(xdata[idx], ydata[idx]-xdata[idx], 
                    xerr = xerr[:,idx], 
                    yerr = yerr[:,idx],
                    fmt='none', ecolor=colors[j])

    # Set limits
    ax.set_xlim(mra)
    ax.set_ylim(deltamra)
        
    # Add 1 to 1 line
    ax.plot(mra, [0,0], 'k--')

    # Change tick labels
    tcks = ax.get_xticks()
    ax.set_xticks(tcks[tcks == np.floor(tcks)])

    # Add / remove axis labels
    if i == nmod-1:
        ax.set_xlabel(r'$\log\,M$ [$M_\odot$]')
    else:
        ax.set_xticklabels([])
    if i != 1:
        tcks = ax.get_yticks()
        ax.set_yticks(tcks[:-1])

    # Add text
    plt.text(5.75, 3.0, labels[i] + ' vs. ' + labels[0],
             horizontalalignment='right',
             bbox={'facecolor': 'white'})

    # Add title
    if i == 1:
        ax.set_title('Mass')

    # Make subplot for age comparison
    ax = fig.add_subplot(nmod-1,2,2*i)

    # Loop over galaxies
    for j in range(ngal-1, -1, -1):

        # Grab data and sort it
        xdata = csfit[j][0]['logt_pct'][2,:]
        xerr = np.array([csfit[j][0]['logt_pct'][2,:]-
                         csfit[j][0]['logt_pct'][0,:],
                         csfit[j][0]['logt_pct'][4,:]-
                         csfit[j][0]['logt_pct'][2,:]])
        idxsrt = np.argsort(xdata)
        xdata = xdata[idxsrt]
        xerr = xerr[:,idxsrt]
        ydata = csfit[j][i]['logt_pct'][2,:]
        yerr = np.array([csfit[j][i]['logt_pct'][2,:]-
                         csfit[j][i]['logt_pct'][0,:],
                         csfit[j][i]['logt_pct'][4,:]-
                         csfit[j][i]['logt_pct'][2,:]])
        ydata = ydata[idxsrt]
        yerr = yerr[:,idxsrt]

        # Plot medians
        ax.plot(xdata, ydata-xdata, colors[j]+symbols[j])

        # Plot error bars on some points
        idx = [0]
        for l in range(len(xdata)):
            if xdata[l] - xdata[idx[-1]] > skip:
                idx.append(l)
        ax.errorbar(xdata[idx], ydata[idx]-xdata[idx], xerr = xerr[:,idx], 
                    yerr = yerr[:,idx],
                    fmt='none', ecolor=colors[j])

    # Set limits
    ax.set_xlim(tra)
    ax.set_ylim(deltatra)
        
    # Add 1 to 1 line
    ax.plot(tra, [0,0], 'k--')

    # Add / remove axis labels
    if i == nmod-1:
        ax.set_xlabel(r'$\log\,T$ [yr]')
    else:
        ax.set_xticklabels([])
    if i != 1:
        tcks = ax.get_yticks()
        ax.set_yticks(tcks[:-1])

    # Add title
    if i == 1:
        ax.set_title('Age')

    # Add legend
    if i == 1:
        ax.legend(ptmp, gallabels, loc='upper right', numpoints=1,
                  prop={'size':10})

    # Move y labels to other side
    ax.yaxis.tick_right()
    ax.yaxis.set_ticks_position('both')

# Adjust subplots
plt.subplots_adjust(hspace=0, wspace=0.05, bottom=0.04, top=0.97)

# Add common y labels
ax = fig.add_subplot(1,2,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel(r'$\Delta\, \log\,M$ [$M_\odot$]', labelpad=18)
ax = fig.add_subplot(1,2,2)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.yaxis.set_label_position('right')
ax.set_ylabel(r'$\Delta \,\log\,T$ [yr]', labelpad=24)

# Save
plt.savefig('modelcomp.pdf')
