# This script plots the distribution of physical properties of
# the LEGUS star clusters for the fiducial case

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']

# Specify fiducial model choices
libdir = '/home/krumholz/pfs/cluster_slug/lib/'
csfile = 'modp020_kroupalim_MW_phi0.50'
fitdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1/'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']
msfid = 2
tsfid = 0.5

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Go through photometry files to find high confidence clusters
classidx = []
classif = []
for nidx, pf in zip(nuc_cl, photfiles):
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    cid = list(data['col1'])
    if nidx in cid:
        classidx[-1][cid.index(nidx)] = False
    classif.append(classification[classidx[-1]])

# Read data
csfit = []
for i in range(len(galnames)):
    fname = osp.join(fitdir,
                     galnames[i]+'_'+osp.basename(csfile) + \
                     '_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.format(msfid, tsfid))
    hdulist = fits.open(fname)
    logm = hdulist[1].data['log_m']
    logt = hdulist[1].data['log_t']
    AV = hdulist[1].data['AV']
    cid = hdulist[2].data['Cluster_ID'][classidx[i]]
    mpdf = hdulist[2].data['Mass_PDF'][classidx[i]]
    tpdf = hdulist[2].data['Age_PDF'][classidx[i]]
    AVpdf = hdulist[2].data['AV_PDF'][classidx[i]]
    dist = hdulist[2].data['Phot_dist'][classidx[i]]
    hdulist.close()

    # Compute percentiles
    mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
    logm_pct = np.array([
        logm[np.argmax(np.greater(mpdfsum, 0.1), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
        logm[np.argmax(np.greater(mpdfsum, 0.9), axis=1)]])
    tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
    logt_pct = np.array([
        logt[np.argmax(np.greater(tpdfsum, 0.1), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
        logt[np.argmax(np.greater(tpdfsum, 0.9), axis=1)]])
    AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
    AV_pct = np.array([
        AV[np.argmax(np.greater(AVpdfsum, 0.1), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
        AV[np.argmax(np.greater(AVpdfsum, 0.9), axis=1)]])

    # Save data
    csfit.append(
        { 'logm' : logm, 'logt' : logt, 'AV' : AV, 
          'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
          'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
          'AV_pct' : AV_pct, 'dist' : dist, 'id' : cid })

# Plot parameters
ncl = 15
pdfra = [0, 1]

# Plot the masses
fig = plt.figure(1, figsize=(5,10))
plt.clf()
logmra = [2, 6.5]

# Make common plot label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel(r'$\log\,M$ [$M_\odot$]', labelpad=18)

# Loop over galaxies
for i, cs in enumerate(csfit):

    # Create axes for this galaxy
    ax = fig.add_subplot(len(csfit),1,i+1)

    # Generate list of clusters to plot from this galaxy
    logmmed = cs['logm_pct'][2,:]
    sortidx = np.argsort(logmmed)
    logmcut = np.linspace(np.amin(logmmed), np.amax(logmmed), ncl)
    subidx = [np.argmax(logmmed[sortidx] >= logmcut[k])
              for k in range(ncl)]
    for k in range(len(subidx)-1,0,-1):
        while np.sum(subidx[k-1] == subidx[k:]) > 0:
            subidx[k-1] = subidx[k-1]-1
    clidx = sortidx[subidx]

    # Loop over clusters
    for j, cl in enumerate(clidx):

        # Make a pseudo-image for this cluster
        img = np.zeros((len(cs['logm']), 2))
        img[:,0] = cs['mpdf'][cl,:]
        img[:,1] = img[:,0]

        # X range
        xra = [(j+0.15)/float(ncl), (j+0.85)/float(ncl)]

        # Show image
        ax.imshow(img, origin='lower', 
                  extent=(xra[0], xra[1], cs['logm'][0], cs['logm'][-1]),
                  vmin=pdfra[0], vmax=pdfra[1],
                  aspect='auto', cmap=cm.Blues)

        # Box for 1st - 3rd quartile
        mpct = cs['logm_pct'][:,cl]
        ax.plot([xra[0], xra[1], xra[1], xra[0], xra[0]],
                [mpct[1], mpct[1], mpct[3], mpct[3], mpct[1]], 'r')

        # Median line
        ax.plot([xra[0], xra[1]], [mpct[2], mpct[2]], 'r')

        # Generate whiskers
        ax.errorbar([0.5*(xra[0]+xra[1])], [mpct[1]],
                    yerr=np.array([mpct[1]-mpct[0], 0]).reshape(2,1),
                    fmt='none', ecolor='r')
        ax.errorbar([0.5*(xra[0]+xra[1])], [mpct[3]],
                    yerr=np.array([0, mpct[4]-mpct[3]]).reshape(2,1),
                    fmt='none', ecolor='r')

    # Set plot range
    ax.set_xlim((0,1))
    ax.set_ylim(logmra)

    # Add label
    plt.text(0.5/ncl, 6.1, gallabels[i], bbox={'facecolor':'white'}, size=10)

    # Fix axis labels
    ax.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[::2])
    ax.set_xticklabels(cs['id'][clidx][::2])
    if i == len(csfit)-1:
        ax.set_xlabel('Cluster ID')
    axtwin = ax.twiny()
    axtwin.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[1::2])
    axtwin.set_xticklabels(cs['id'][clidx][1::2])
    if i == 0:
        axtwin.set_xlabel('Cluster ID')

# Adjust spacing and add colorbar
plt.subplots_adjust(bottom=0.07, top=0.93, right=0.82)
axcbar = fig.add_axes((0.84, 0.07, 0.04, 0.93-0.07))
norm = mpl.colors.Normalize(vmin=pdfra[0], vmax=pdfra[1])
cbar = mpl.colorbar.ColorbarBase(axcbar, cmap=cm.Blues, norm=norm,
                                 extend='max')
cbar.set_label('Probability density')

# Save
plt.savefig('mass_pdfs.pdf')


# Plot the times
fig = plt.figure(2, figsize=(5,10))
plt.clf()
logtra = [5, 10.2]

# Make common plot label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel(r'$\log\,T$ [yr]', labelpad=12)

# Loop over galaxies
for i, cs in enumerate(csfit):

    # Create axes for this galaxy
    ax = fig.add_subplot(len(csfit),1,i+1)

    # Generate list of clusters to plot from this galaxy
    logtmed = cs['logt_pct'][2,:]
    sortidx = np.argsort(logtmed)
    logtcut = np.linspace(np.amin(logtmed), np.amax(logtmed), ncl)
    subidx = [np.argmax(logtmed[sortidx] >= logtcut[k])
              for k in range(ncl)]
    for k in range(len(subidx)-1,0,-1):
        while np.sum(subidx[k-1] == subidx[k:]) > 0:
            subidx[k-1] = subidx[k-1]-1
    clidx = sortidx[subidx]

    # Loop over clusters
    for j, cl in enumerate(clidx):

        # Make a pseudo-image for this cluster
        img = np.zeros((len(cs['logt']), 2))
        img[:,0] = cs['tpdf'][cl,:]
        img[:,1] = img[:,0]

        # X range
        xra = [(j+0.15)/float(ncl), (j+0.85)/float(ncl)]

        # Show image
        ax.imshow(img, origin='lower', 
                  extent=(xra[0], xra[1], cs['logt'][0], cs['logt'][-1]),
                  vmin=pdfra[0], vmax=pdfra[1],
                  aspect='auto', cmap=cm.Blues)

        # Box for 1st - 3rd quartile
        tpct = cs['logt_pct'][:,cl]
        ax.plot([xra[0], xra[1], xra[1], xra[0], xra[0]],
                [tpct[1], tpct[1], tpct[3], tpct[3], tpct[1]], 'r')

        # Median line
        ax.plot([xra[0], xra[1]], [tpct[2], tpct[2]], 'r')

        # Generate whiskers
        ax.errorbar([0.5*(xra[0]+xra[1])], [tpct[1]],
                    yerr=np.array([tpct[1]-tpct[0], 0]).reshape(2,1),
                    fmt='none', ecolor='r')
        ax.errorbar([0.5*(xra[0]+xra[1])], [tpct[3]],
                    yerr=np.array([0, tpct[4]-tpct[3]]).reshape(2,1),
                    fmt='none', ecolor='r')

    # Set plot range
    ax.set_xlim((0,1))
    ax.set_ylim(logtra)

    # Add label
    plt.text(0.5/ncl, 9.5, gallabels[i], bbox={'facecolor':'white'}, size=10)

    # Fix axis labels
    ax.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[::2])
    ax.set_xticklabels(cs['id'][clidx][::2])
    if i == len(csfit)-1:
        ax.set_xlabel('Cluster ID')
    axtwin = ax.twiny()
    axtwin.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[1::2])
    axtwin.set_xticklabels(cs['id'][clidx][1::2])
    if i == 0:
        axtwin.set_xlabel('Cluster ID')

# Adjust spacing and add colorbar
plt.subplots_adjust(bottom=0.07, top=0.93, right=0.82)
axcbar = fig.add_axes((0.84, 0.07, 0.04, 0.93-0.07))
norm = mpl.colors.Normalize(vmin=pdfra[0], vmax=pdfra[1])
cbar = mpl.colorbar.ColorbarBase(axcbar, cmap=cm.Blues, norm=norm,
                                 extend='max')
cbar.set_label('Probability density')

# Save
plt.savefig('age_pdfs.pdf')


# Plot the A_V's
fig = plt.figure(3, figsize=(5,10))
plt.clf()
avra = [0, 3]

# Make common plot label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel(r'$A_V$ [mag]', labelpad=18)

# Loop over galaxies
for i, cs in enumerate(csfit):

    # Create axes for this galaxy
    ax = fig.add_subplot(len(csfit),1,i+1)

    # Generate list of clusters to plot from this galaxy
    AVmed = cs['AV_pct'][2,:]
    sortidx = np.argsort(AVmed)
    AVcut = np.linspace(np.amin(AVmed), np.amax(AVmed), ncl)
    subidx = [np.argmax(AVmed[sortidx] >= AVcut[k])
              for k in range(ncl)]
    for k in range(len(subidx)-1,0,-1):
        while np.sum(subidx[k-1] == subidx[k:]) > 0:
            subidx[k-1] = subidx[k-1]-1
    clidx = sortidx[subidx]

    # Loop over clusters
    for j, cl in enumerate(clidx):

        # Make a pseudo-image for this cluster
        img = np.zeros((len(cs['AV']), 2))
        img[:,0] = cs['AVpdf'][cl,:]
        img[:,1] = img[:,0]

        # X range
        xra = [(j+0.15)/float(ncl), (j+0.85)/float(ncl)]

        # Show image
        ax.imshow(img, origin='lower', 
                  extent=(xra[0], xra[1], cs['AV'][0], cs['AV'][-1]),
                  vmin=pdfra[0], vmax=pdfra[1],
                  aspect='auto', cmap=cm.Blues)

        # Box for 1st - 3rd quartile
        AVpct = cs['AV_pct'][:,cl]
        ax.plot([xra[0], xra[1], xra[1], xra[0], xra[0]],
                [AVpct[1], AVpct[1], AVpct[3], AVpct[3], AVpct[1]], 'r')

        # Median line
        ax.plot([xra[0], xra[1]], [AVpct[2], AVpct[2]], 'r')

        # Generate whiskers
        ax.errorbar([0.5*(xra[0]+xra[1])], [AVpct[1]],
                    yerr=np.array([AVpct[1]-AVpct[0], 0]).reshape(2,1),
                    fmt='none', ecolor='r')
        ax.errorbar([0.5*(xra[0]+xra[1])], [AVpct[3]],
                    yerr=np.array([0, AVpct[4]-AVpct[3]]).reshape(2,1),
                    fmt='none', ecolor='r')

    # Set plot range
    ax.set_xlim((0,1))
    ax.set_ylim(avra)

    # Add label
    plt.text(0.5/ncl, 2.75, gallabels[i], bbox={'facecolor':'white'}, size=10)

    # Fix axis labels
    ax.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[::2])
    ax.set_xticklabels(cs['id'][clidx][::2])
    if i == len(csfit)-1:
        ax.set_xlabel('Cluster ID')
    axtwin = ax.twiny()
    axtwin.set_xticks(np.linspace(0.5/ncl, (ncl-0.5)/ncl, ncl)[1::2])
    axtwin.set_xticklabels(cs['id'][clidx][1::2])
    if i == 0:
        axtwin.set_xlabel('Cluster ID')

# Adjust spacing and add colorbar
plt.subplots_adjust(bottom=0.07, top=0.93, right=0.82)
axcbar = fig.add_axes((0.84, 0.07, 0.04, 0.93-0.07))
norm = mpl.colors.Normalize(vmin=pdfra[0], vmax=pdfra[1])
cbar = mpl.colorbar.ColorbarBase(axcbar, cmap=cm.Blues, norm=norm,
                                 extend='max')
cbar.set_label('Probability density')

# Save
plt.savefig('AV_pdfs.pdf')


