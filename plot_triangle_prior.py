# This script makes a triangle plot for an example cluster that shows
# a bimodal posterior PDF in age

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as colors
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii
from slugpy.cluster_slug import cluster_slug

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']

# Distance mods
distmod = [27.68, 27.72, 29.98]

# Specify fiducial cluster_slug model choices
csfile = '/home/krumholz/pfs/cluster_slug/lib/modp020_kroupalim_MW_phi0.50'
libdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1/'
libname = 'pad_020_kroupa_MW'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC628e']
msfid = 2
tsfid = 0.5

# List of filters
filters = [ ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
             'ACS_F555W', 'ACS_F814W'],
            ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'WFC3_UVIS_F438W',
             'WFC3_UVIS_F555W', 'WFC3_UVIS_F814W'],
            ['WFC3_UVIS_F275W', 'WFC3_UVIS_F336W', 'ACS_F435W',
             'WFC3_UVIS_F555W', 'ACS_F814W'] ]

# Create the required cluster_slug object
# Define the priors
class priorfunc(object):
    def __init__(self, massslope, ageslope, logageflat=6.5):
        self.massslope = massslope
        self.ageslope = ageslope
        self.logtflat = logageflat
    def prior(self, physprop):
        logm = physprop[:,0]
        logt = physprop[:,1]
        massprior = (10.**logm)**(self.massslope+1)
        timeprior = (10.**(logt-self.logtflat))**(self.ageslope+1)
        timeprior[logt < self.logtflat] \
            = 10.**(logt[logt < self.logtflat]-self.logtflat)
        return massprior*timeprior
priorfid = priorfunc(-msfid, -tsfid)

# Define the density of samples in the library
def sample_density(physprop):
    logm = physprop[:,0]
    logt = physprop[:,1]
    sden = np.ones(len(logm))
    sden[logm > 4] = sden[logm > 4] * 1.0/10.**(logm[logm > 4]-4)
    sden[logt > 8] = sden[logt > 8] * 1.0/10.**(logt[logt > 8]-8)
    return sden

# Read the photometry files, pruning objects that aren't really clusters
classidx = []
phot = []
photerr = []
detect = []
for j, pf in enumerate(photfiles):
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    cid = data['col1']
    phottmp = np.zeros((len(cid), 5))
    photerrtmp = np.zeros((len(cid), 5))
    detecttmp = np.ones((len(cid), 5), dtype=bool)
    for i in range(5):
        phottmp[:,i] = data['col{:d}'.format(2*i+6)] - distmod[j]
        photerrtmp[:,i] = data['col{:d}'.format(2*i+7)]
        detecttmp[:,i] = data['col{:d}'.format(2*i+6)] < 99.  # Flag value
    phot.append(phottmp[classidx[-1],:])
    photerr.append(photerrtmp[classidx[-1],:])
    detect.append(detecttmp[classidx[-1],:])

# Read data
csfit = []
for i in range(len(galnames)):
    fname = osp.join(libdir,
        galnames[i]+'_'+osp.basename(csfile) + \
        '_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.format(msfid, tsfid))
    hdulist = fits.open(fname)
    logm = hdulist[1].data['log_m']
    logt = hdulist[1].data['log_t']
    AV = hdulist[1].data['AV']
    cid = hdulist[2].data['Cluster_ID'][classidx[i]]
    mpdf = hdulist[2].data['Mass_PDF'][classidx[i]]
    tpdf = hdulist[2].data['Age_PDF'][classidx[i]]
    AVpdf = hdulist[2].data['AV_PDF'][classidx[i]]
    dist = hdulist[2].data['Phot_dist'][classidx[i]]
    hdulist.close()

    # Save data
    csfit.append(
        { 'logm' : logm, 'logt' : logt, 'AV' : AV, 
          'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
          'id' : cid, 'dist' : dist })

# Get 2D histogram for our chosen example
galidx = 2
cidx = np.where(csfit[galidx]['id'] == 320)[0][0]
logmlim = [0, 6]
logtlim = [6, 9.5]
AVlim = [0, 3]

# Instantiate the cluster_slug object
cs = cluster_slug(photsystem='Vega', filters=filters[galidx], 
                  priors=priorfid.prior,
                  libname=csfile, sample_density=sample_density,
                  abstol = 1.0e-10, bw_phot=0.1)

# Make 1D and 2D pdfs for fiducial case
ngrid = 128
print "Doing fiducial case..."
cs.priors = priorfid.prior
print "Making M-T histogram..."
mtgrid, mtpdf = cs.mpdf([0,1], phot[galidx][cidx,:],
                        photerr[galidx][cidx,:],
                        qmin=[logmlim[0], logtlim[0]],
                        qmax=[logmlim[1], logtlim[1]],
                        ngrid=ngrid)
print "Making M-AV histogram..."
mavgrid, mavpdf = cs.mpdf([0,2], phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=[logmlim[0], AVlim[0]],
                          qmax=[logmlim[1], AVlim[1]],
                          ngrid=ngrid)
print "Making T-AV histogram..."
tavgrid, tavpdf = cs.mpdf([1,2], phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=[logtlim[0], AVlim[0]],
                          qmax=[logtlim[1], AVlim[1]],
                          ngrid=ngrid)
print "Making 1D pdfs"
mgrid, mpdf_fid = cs.mpdf(0, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logmlim[0], qmax=logmlim[1],
                          ngrid=ngrid)
tgrid, tpdf_fid = cs.mpdf(1, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logtlim[0], qmax=logtlim[1],
                          ngrid=ngrid)
AVgrid, AVpdf_fid = cs.mpdf(2, phot[galidx][cidx,:],
                            photerr[galidx][cidx,:],
                            qmin=AVlim[0], qmax=AVlim[1],
                            ngrid=ngrid)

# Now change priors and do the other cases
priorvar = priorfunc(-2.0, -1.0)
cs.priors = priorvar.prior
print "Doing case with varying priors..."
print "Making M-T histogram..."
mtgrid, mtpdf_var = cs.mpdf([0,1], phot[galidx][cidx,:],
                            photerr[galidx][cidx,:],
                            qmin=[logmlim[0], logtlim[0]],
                            qmax=[logmlim[1], logtlim[1]],
                            ngrid=ngrid)
print "Making M-AV histogram..."
mavgrid, mavpdf_var = cs.mpdf([0,2], phot[galidx][cidx,:],
                              photerr[galidx][cidx,:],
                              qmin=[logmlim[0], AVlim[0]],
                              qmax=[logmlim[1], AVlim[1]],
                              ngrid=ngrid)
print "Making T-AV histogram..."
tavgrid, tavpdf_var = cs.mpdf([1,2], phot[galidx][cidx,:],
                              photerr[galidx][cidx,:],
                              qmin=[logtlim[0], AVlim[0]],
                              qmax=[logtlim[1], AVlim[1]],
                              ngrid=ngrid)
print "Making 1D pdfs"
mgrid, mpdf_var = cs.mpdf(0, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logmlim[0], qmax=logmlim[1],
                          ngrid=ngrid)
tgrid, tpdf_var = cs.mpdf(1, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logtlim[0], qmax=logtlim[1],
                          ngrid=ngrid)
AVgrid, AVpdf_var = cs.mpdf(2, phot[galidx][cidx,:],
                            photerr[galidx][cidx,:],
                            qmin=AVlim[0], qmax=AVlim[1],
                            ngrid=ngrid)


priorvar = priorfunc(-2.0, 0.0)
cs.priors = priorvar.prior
print "Doing case with varying priors (2)..."
print "Making M-T histogram..."
mtgrid, mtpdf_var1 = cs.mpdf([0,1], phot[galidx][cidx,:],
                            photerr[galidx][cidx,:],
                            qmin=[logmlim[0], logtlim[0]],
                            qmax=[logmlim[1], logtlim[1]],
                            ngrid=ngrid)
print "Making M-AV histogram..."
mavgrid, mavpdf_var1 = cs.mpdf([0,2], phot[galidx][cidx,:],
                              photerr[galidx][cidx,:],
                              qmin=[logmlim[0], AVlim[0]],
                              qmax=[logmlim[1], AVlim[1]],
                              ngrid=ngrid)
print "Making T-AV histogram..."
tavgrid, tavpdf_var1 = cs.mpdf([1,2], phot[galidx][cidx,:],
                              photerr[galidx][cidx,:],
                              qmin=[logtlim[0], AVlim[0]],
                              qmax=[logtlim[1], AVlim[1]],
                              ngrid=ngrid)
print "Making 1D pdfs"
mgrid, mpdf_var1 = cs.mpdf(0, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logmlim[0], qmax=logmlim[1],
                          ngrid=ngrid)
tgrid, tpdf_var1 = cs.mpdf(1, phot[galidx][cidx,:],
                          photerr[galidx][cidx,:],
                          qmin=logtlim[0], qmax=logtlim[1],
                          ngrid=ngrid)
AVgrid, AVpdf_var1 = cs.mpdf(2, phot[galidx][cidx,:],
                            photerr[galidx][cidx,:],
                            qmin=AVlim[0], qmax=AVlim[1],
                            ngrid=ngrid)

# Set up color maps for plots
cdictr = { 'red' : ((0.0, 0.0, 1.0),
                    (1.0, 1.0, 0.0)),
           'green' : ((0.0, 0.0, 1.0),
                      (1.0, 0.0, 0.0)),
           'blue' : ((0.0, 0.0, 1.0),
                     (1.0, 0.0, 0.0)),
           'alpha' : ((0.0, 0.0, 0.0),
                      (1.0, 1.0, 0.0)) }
cmr = LinearSegmentedColormap('WhiteRed', cdictr)
cdictb = { 'red' : ((0.0, 0.0, 1.0),
                    (1.0, 0.0, 0.0)),
           'green' : ((0.0, 0.0, 1.0),
                      (1.0, 0.0, 0.0)),
           'blue' : ((0.0, 0.0, 1.0),
                     (1.0, 1.0, 0.0)),
           'alpha' : ((0.0, 0.0, 0.0),
                      (1.0, 1.0, 0.0)) }
cmb = LinearSegmentedColormap('WhiteBlue', cdictb)

# Plot posteriors PDFs
fig = plt.figure(1, figsize=(8, 8))
plt.clf()

# Set limits
mra = [np.floor(mtgrid[0,0,0]), np.ceil(mtgrid[0,-1,-1])]
tra = [np.floor(mtgrid[1,0,0]), np.ceil(mtgrid[1,-1,-1])]
avra = [np.floor(mavgrid[1,0,0]), np.ceil(mavgrid[1,-1,-1])]
pdfra = [0, 3]
logpdfra = [-3, 0.5]
cntstep = 0.4
clevels = np.arange(0, np.amax(tavpdf_var), cntstep)

# Marginal mass PDFs
ax = fig.add_subplot(3,3,1)
ax.plot(mgrid, mpdf_var1, 'b')
ax.plot(mgrid, mpdf_fid, 'k', lw=2)
ax.plot(mgrid, mpdf_var, 'r')
ax.set_xlim(mra)
ax.set_ylim(pdfra)
ax.set_xticklabels([])
ax.yaxis.tick_right()
ax.yaxis.set_label_position("right")
ax.yaxis.set_ticks_position('both')
ax.set_ylabel('PDF')
ax.set_yticks(np.linspace(pdfra[0], pdfra[1], 7)[1:])
tcks = ax.get_xticks()
ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])
ax.legend([r'$\gamma=0$', r'$\gamma=-0.5$', r'$\gamma=-1.0$'],
          loc='upper left')

# Joint mass-age PDF
ax = fig.add_subplot(3,3,4)
ax.contourf(mtgrid[0,:,:], mtgrid[1,:,:], 
            np.log10(mtpdf_var1), 
            cmap=cmb, 
            levels=np.linspace(-1,1,50))
ax.contourf(mtgrid[0,:,:], mtgrid[1,:,:], 
            np.log10(mtpdf_var), 
            cmap=cmr, 
            levels=np.linspace(-1,1,50))
ax.contour(mtgrid[0,:,:], mtgrid[1,:,:], np.log10(mtpdf),
           colors=['k']*5,
           levels=np.linspace(-1,1,5))
ax.set_xlim(mra)
ax.set_ylim(tra)
ax.set_xticklabels([])
ax.set_ylabel(r'$\log\,T$ [yr]')
tcks = ax.get_xticks()
ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])

# Marginal age PDF
ax = fig.add_subplot(3,3,5)
ax.plot(tgrid, tpdf_var1, 'b')
ax.plot(tgrid, tpdf_fid, 'k', lw=2)
ax.plot(tgrid, tpdf_var, 'r')
ax.set_xlim(tra)
ax.set_ylim(pdfra)
ax.set_xticklabels([])
ax.yaxis.tick_right()
ax.yaxis.set_label_position("right")
ax.yaxis.set_ticks_position('both')
ax.set_ylabel('PDF')
ax.set_yticks(np.linspace(pdfra[0], pdfra[1], 7)[1:])
tcks = ax.get_xticks()
ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])

# Joint mass-AV PDF
ax = fig.add_subplot(3,3,7)
ax.contourf(mavgrid[0,:,:], mavgrid[1,:,:], 
            np.log10(mavpdf_var1), 
            cmap=cmb, 
            levels=np.linspace(-1,1,50))
ax.contourf(mavgrid[0,:,:], mavgrid[1,:,:], 
            np.log10(mavpdf_var), 
            cmap=cmr, 
            levels=np.linspace(-1,1,50))
ax.contour(mavgrid[0,:,:], mavgrid[1,:,:], np.log10(mavpdf),
           colors=['k']*5,
           levels=np.linspace(-1,1,5))
ax.set_xlim(mra)
ax.set_ylim(avra)
ax.set_xlabel('$\log\,M$ [$M_\odot$]')
ax.set_ylabel('$A_V$ [mag]')
tcks = ax.get_xticks()
ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])
tcks = ax.get_yticks()
ax.set_yticks(tcks[:-1])

# Joint age-AV PDF
ax = fig.add_subplot(3,3,8)
ax.contourf(tavgrid[0,:,:], tavgrid[1,:,:], 
            np.log10(tavpdf_var1), 
            cmap=cmb, 
            levels=np.linspace(-1,1,50))
ax.contourf(tavgrid[0,:,:], tavgrid[1,:,:], 
            np.log10(tavpdf_var), 
            cmap=cmr, 
            levels=np.linspace(-1,1,50))
ax.contour(tavgrid[0,:,:], tavgrid[1,:,:], np.log10(tavpdf),
           colors=['k']*5,
           levels=np.linspace(-1,1,5))
ax.set_xlim(tra)
ax.set_ylim(avra)
ax.set_yticklabels([])
ax.set_xlabel(r'$\log\,T$ [yr]')
tcks = ax.get_xticks()
ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])

# Marginal AV PDF
ax = fig.add_subplot(3,3,9)
ax.plot(AVgrid, AVpdf_var1, 'b')
ax.plot(AVgrid, AVpdf_fid, 'k', lw=2)
ax.plot(AVgrid, AVpdf_var, 'r')
ax.set_xlim(avra)
ax.set_ylim(pdfra)
ax.yaxis.tick_right()
ax.yaxis.set_label_position("right")
ax.yaxis.set_ticks_position('both')
ax.set_ylabel('PDF')
ax.set_xlabel('$A_V$ [mag]')
ax.set_yticks(np.linspace(pdfra[0], pdfra[1], 7))

# Remove space
plt.subplots_adjust(wspace=0, hspace=0, bottom=0.08, top=0.96,
                    left=0.12, right=0.9)

# Add colorbars
pos = ax.get_position()
axcbar1 = fig.add_axes((0.49*(pos.x0+pos.x1), 2*pos.y1 - pos.y0,
                       0.02, pos.y1 - pos.y0))
axcbar2 = fig.add_axes((0.49*(pos.x0+pos.x1)+0.02, 2*pos.y1 - pos.y0,
                        0.02, pos.y1 - pos.y0))
norm = mpl.colors.Normalize(vmin=-1, vmax=1)
cbar1 = mpl.colorbar.ColorbarBase(axcbar1, cmap=cmr, norm=norm)
cbar2 = mpl.colorbar.ColorbarBase(axcbar2, cmap=cmb, norm=norm)
cbar1.set_ticks([])
cbar2.set_label(r'log PDF')
for l in np.linspace(-1,1,5):
    axcbar1.plot([0,1], [norm(l), norm(l)], 'k')
    axcbar2.plot([0,1], [norm(l), norm(l)], 'k')

# Save
plt.savefig('triangle2.pdf')
