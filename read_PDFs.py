"""
This module contains a routine, read_PDFs, that can parse the FITS
files containing the posterior PDFs for LEGUS clusters.
"""

from astropy.io import fits
import numpy as np

def read_PDFs(fname):
    """
    This routine reads the posterior PDFs stored in a LEGUS posterior
    PDF output file into memory in a convenient format. Files to be
    read can contain 1D, 2D, or 3D PDFs.

    Parameters:
       fname : string
          name of file to be read

    Returns:
       A dict containing the following keys:

       log_m : array, shape (I)
          array of I log masses (in Msun) at which posterior PDFs are
          computed
       log_t : array, shape (J)
          array of J log ages (in yr) at which posterior PDFs are
          computed
       Cluster_ID : array, shape (N)
          ID numbers for all N clusters in the file being read

       If present in the file being read:

       Phot_dist : array, shape (N, M)
          photometric distances to the nearest M library clusters to
          each of the N observed clusters
       Phot_dist_norm : array, shape (N, M)
          error-normalised photometric distances to the nearest M
          library clusters to each of the N observed clusters

       Present only for 1D posterior PDF files:

       AV : array, shape (K)
          array of K visual extinctions (in mag) at which posterior PDFs
          are computed
       Mass_PDF : array, shape (N, I)
          marginal posterior PDF for cluster mass for all N clusters
          in the file, evaluated at I values of log mass
       Age_PDF : array, shape (N, J)
          marginal posterior PDF for cluster mass for all N clusters
          in the file, evaluated at J values of log age
       AV_PDF : array, shape (N, K)
          marginal posterior PDF for cluster mass for all N clusters
          in the file, evaluated at K values of AV

       Present only for 2D posterior PDF files:

       PDF : array, shape (N, I, J)
          posterior PDF evaluated at each point in (log_m, log_t); PDF
          is marginalised over extinction

       Present only for 3D posterior PDF files:

       AV : array, shape (K)
          array of K visual extinctions (in mag) at which posterior PDFs
          are computed
       PDF : array, shape (N, I, J, K)
          posterior PDF evaluated at each point in (log_m, log_t, AV)
    """

    # Open the input file
    hdulist = fits.open(fname)

    # Empty dict that we will fill
    out = {}

    # Extract the grids in log_m, log_t, AV
    out['log_m'] = np.array(hdulist[1].data['log_m'])
    out['log_t'] = np.array(hdulist[1].data['log_t'])
    if 'AV' in hdulist[1].columns.names:
        out['AV'] = np.array(hdulist[1].data['AV'])

    # Extract cluster IDs
    out['Cluster_ID'] = np.array(hdulist[2].data['Cluster_ID'])
    
    # Extract photometric distances
    if 'Phot_dist' in hdulist[2].columns.names:
        out['Phot_dist'] = np.array(hdulist[2].data['Phot_dist'])
        out['Phot_dist_norm'] = np.array(hdulist[2].data['Phot_dist_norm'])

    # Figure out how many dimensions the FITS file has, and extract
    # the PDFs
    if 'Mass_PDF' in hdulist[2].columns.names:
        # 1D file
        out['Mass_PDF'] = np.array(hdulist[2].data['Mass_PDF'])
        out['Age_PDF'] = np.array(hdulist[2].data['Age_PDF'])
        out['AV_PDF'] = np.array(hdulist[2].data['AV_PDF'])
    elif 'PDF' in hdulist[2].columns.names:
        # 2D or 3D file
        pdf = np.array(hdulist[2].data['PDF'])
        if pdf.shape[1] == out['log_m'].size * out['log_t'].size:
            # 2D file
            out['PDF'] = pdf.reshape((
                out['Cluster_ID'].size, out['log_m'].size,
                out['log_t'].size))
        else:
            # 3D file
            out['PDF'] = pdf.reshape((
                out['Cluster_ID'].size, out['log_m'].size,
                out['log_t'].size, out['AV'].size))

    # Close file
    hdulist.close()

    # Return
    return out
