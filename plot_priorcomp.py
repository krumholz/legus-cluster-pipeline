# This script makes plots comparing the results obtained with
# different priors on the cluster mass and age distributions

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii
import glob

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
distmod = [27.68, 27.72, 29.98]

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Specify cluster_slug models to use
csfile = '/home/krumholz/pfs/cluster_slug/lib/modp020_kroupalim_MW_phi0.50'
libdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1'
libname = 'pad_020_kroupa_MW'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']

# Go through photometry files to find high confidence clusters
classif = []
classidx = []
phot = []
photerr = []
photerrmean = []
detect = []
for dmod, nidx, pf in zip(distmod, nuc_cl, photfiles):
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))
    cid = list(data['col1'])
    if nidx in cid:
        classidx[-1][cid.index(nidx)] = False
    classif.append(classification[classidx[-1]])
    phottmp = np.zeros((len(cid), 5))
    photerrtmp = np.zeros((len(cid), 5))
    detecttmp = np.ones((len(cid), 5), dtype=bool)
    for i in range(5):
        phottmp[:,i] = data['col{:d}'.format(2*i+6)] - dmod
        photerrtmp[:,i] = data['col{:d}'.format(2*i+7)]
        detecttmp[:,i] = data['col{:d}'.format(2*i+6)] < 99.  # Flag value
    phot.append(phottmp[classidx[-1],:])
    photerr.append(photerrtmp[classidx[-1],:])
    detect.append(detecttmp[classidx[-1],:])
    photerr[-1][np.logical_not(detect[-1])] = 0.0  # Null out non-detections
    photerrmean.append(np.sqrt(np.sum(photerr[-1]**2, axis=1)) /
                       np.sqrt(np.sum(photerr[-1] != 0.0, axis=1)))

# Read the posterior marginal PDFs from cluster_slug for various
# priors; again, strip the things that are not classified as
# well-detected clusters
csfit = []
for j in range(len(galnames)):

    # Get all output files for this galaxy
    files = glob.glob(
        osp.join(libdir,
                 galnames[j]+'_'+osp.basename(csfile) +
                 '_ms???_ts???_pdf_1D.fits'))
    csgal = []
    for i, f1 in enumerate(files):

        # Strip directory
        f = osp.basename(f1)

        # Read the extinction curve we're using
        ext = f.split('_')[3]

        # Read data
        hdulist = fits.open(f1)
        logm = hdulist[1].data['log_m']
        logt = hdulist[1].data['log_t']
        AV = hdulist[1].data['AV']
        mpdf = hdulist[2].data['Mass_PDF'][classidx[j]]
        tpdf = hdulist[2].data['Age_PDF'][classidx[j]]
        AVpdf = hdulist[2].data['AV_PDF'][classidx[j]]
        dist = hdulist[2].data['Phot_dist'][classidx[j]]
        msprior = -float(f.split('ms')[1][:3])
        tsprior = -float(f.split('ts')[1][:3])
        hdulist.close()

        # Compute percentiles
        mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
        logm_pct = np.array([
            logm[np.argmax(np.greater(mpdfsum, 0.1), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.9), axis=1)]])
        tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
        logt_pct = np.array([
            logt[np.argmax(np.greater(tpdfsum, 0.1), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.9), axis=1)]])
        AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
        AV_pct = np.array([
            AV[np.argmax(np.greater(AVpdfsum, 0.1), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.9), axis=1)]])

        # Sum distributions to get whole population distribution
        mpdfsum = np.sum(mpdf, axis=0)
        mpdfsum = mpdfsum / (np.sum(mpdfsum)*(logm[1]-logm[0]))
        tpdfsum = np.sum(tpdf, axis=0)
        tpdfsum = tpdfsum / (np.sum(tpdfsum)*(logt[1]-logt[0]))
        AVpdfsum = np.sum(AVpdf, axis=0)
        AVpdfsum = AVpdfsum / (np.sum(AVpdfsum)*(AV[1]-AV[0]))

        # Save data
        csgal.append(
            { 'msprior' : msprior, 'tsprior' : tsprior,
              'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
              'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
              'AV_pct' : AV_pct, 'dist' : dist,
              'ext' : ext, 'logm' : logm, 'logt' : logt,
              'AV' : AV, 'mpdfsum' : mpdfsum, 'tpdfsum' : tpdfsum,
              'AVpdfsum' : AVpdfsum })

    # Save
    csfit.append(csgal)

# Swap around to put the fiducial case first
msfid = -2.0
tsfid = -0.5
for i in range(len(csfit)):
    for j in range(len(csfit[i])):
        if (csfit[i][j]['msprior'] == msfid) and \
           (csfit[i][j]['tsprior'] == tsfid):
            tmp = csfit[i].pop(j)
            break
    csfit[i].insert(0, tmp)

# Plot mass vs. mass
fig = plt.figure(1, figsize=(10,10))
plt.clf()
symbols = ['s', '^', 'o']
colors = ['b', 'g', 'r']
mra = [2, 7]
deltamra = [-4,4]
skip = 0.0
skip1 = 0.3
colorra = [0, 0.3]

# Loop over combinations of priors
ngal = len(csfit)
nprior = len(csfit[0])
for i in range(nprior-1):
    for j in range(i+1,nprior):

        # Make subplot
        ax = fig.add_subplot(nprior-1, nprior-1, 
                             i + (j-1)*(nprior-1) + 1)

        # Loop over galaxies
        pgal = []
        for k in range(ngal-1, -1, -1):

            # Grab the data and sort it
            xdata = csfit[k][i]['logm_pct'][2,:]
            ydata = csfit[k][j]['logm_pct'][2,:]
            xerr = np.array([csfit[k][i]['logm_pct'][2,:]-
                             csfit[k][i]['logm_pct'][0,:],
                             csfit[k][i]['logm_pct'][4,:]-
                             csfit[k][i]['logm_pct'][2,:]])
            yerr = np.array([csfit[k][j]['logm_pct'][2,:]-
                             csfit[k][j]['logm_pct'][0,:],
                             csfit[k][j]['logm_pct'][4,:]-
                             csfit[k][j]['logm_pct'][2,:]])
            pe = photerrmean[k]
            dist = csfit[k][i]['dist'][:,4]

            idx = np.argsort(xdata)
            xdata = xdata[idx]
            ydata = ydata[idx]
            xerr = xerr[:,idx]
            yerr = yerr[:,idx]
            pe = pe[idx]

            # Limit how many points we show
            idx = [0]
            for l in range(len(xdata)):
                if xdata[l] - xdata[idx[-1]] > skip:
                    idx.append(l)

            # Plot medians
            p,=ax.plot(xdata[idx], ydata[idx]-xdata[idx], 
                       colors[k]+symbols[k])
            pgal.append(p)

            # Limit how many points we show
            idx = [0]
            for l in range(len(xdata)):
                if xdata[l] - xdata[idx[-1]] > skip1:
                    idx.append(l)

            # Plot 10th - 90th percentile
            ax.errorbar(xdata[idx], ydata[idx]-xdata[idx], 
                        xerr = xerr[:,idx], 
                        yerr = yerr[:,idx],
                        fmt='none', ecolor=colors[k])

        # Set limits
        ax.set_xlim(mra)
        ax.set_ylim(deltamra)

        # Add 1 to 1 line
        ax.plot(mra, [0,0], 'k--')

        # Add labels
        if i == j - 1:
            ax2 = ax.twinx()
            ax2.set_ylabel(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.
                           format(int(csfit[0][j]['msprior']),
                                  csfit[0][j]['tsprior']),
                           labelpad=10)
            ax2.set_yticks([])
            ax3 = ax.twiny()
            ax3.set_xlabel(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.
                           format(int(csfit[0][i]['msprior']),
                                  csfit[0][i]['tsprior']))
            ax3.set_xticks([])

        # Fix tick labels
        if i != 0:
            ax.set_yticklabels([])
        else:
            tcks = ax.get_yticks()
            if j == 1:
                ax.set_yticks(tcks[tcks == np.floor(tcks)])
            else:
                ax.set_yticks(tcks[tcks == np.floor(tcks)][:-1])
        if j != nprior-1:
            ax.set_xticklabels([])
        else:
            tcks = ax.get_xticks()
            if i == nprior-2:
                ax.set_xticks(tcks[tcks == np.floor(tcks)])
            else:
                ax.set_xticks(tcks[tcks == np.floor(tcks)][:-1])

# Fix spacing
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.07, left=0.07, 
                    right=0.95, top=0.95)

# Make common plot labels
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_xlabel(r'$\log\,M$ [$M_\odot$]', labelpad=24)
ax.set_ylabel(r'$\Delta \,\log\,M$ [$M_\odot$]', labelpad=18)

# Put thick box around fiducial column
ax = fig.add_subplot(1,nprior-1,1)
ax.patch.set_facecolor('None')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.spines['top'].set_linewidth(4)
ax.spines['bottom'].set_linewidth(4)
ax.spines['left'].set_linewidth(4)
ax.spines['right'].set_linewidth(4)
ax.set_xticklabels([])
ax.set_yticklabels([])

# Add legend
ax = fig.add_subplot(1,3,3)
ax.patch.set_facecolor('None')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.legend(pgal, gallabels, loc='upper left', numpoints=1)

# Save
plt.savefig('prior_mass.pdf')


# Plot time vs. time
fig = plt.figure(2, figsize=(10,10))
plt.clf()
tra = [5.5, 10.5]
deltatra = [-4, 4]
skip = 0.0

# Loop over combinations of priors
ngal = len(csfit)
nprior = len(csfit[0])
for i in range(nprior-1):
    for j in range(i+1,nprior):

        # Make subplot
        ax = fig.add_subplot(nprior-1, nprior-1, 
                             i + (j-1)*(nprior-1) + 1)

        # Loop over galaxies
        pgal = []
        for k in range(ngal-1,-1,-1):

            # Grab the data and sort it
            xdata = csfit[k][i]['logt_pct'][2,:]
            ydata = csfit[k][j]['logt_pct'][2,:]
            xerr = np.array([csfit[k][i]['logt_pct'][2,:]-
                             csfit[k][i]['logt_pct'][0,:],
                             csfit[k][i]['logt_pct'][4,:]-
                             csfit[k][i]['logt_pct'][2,:]])
            yerr = np.array([csfit[k][j]['logt_pct'][2,:]-
                             csfit[k][j]['logt_pct'][0,:],
                             csfit[k][j]['logt_pct'][4,:]-
                             csfit[k][j]['logt_pct'][2,:]])
            pe = photerrmean[k]
            dist = csfit[k][i]['dist'][:,4]
            idx = np.argsort(xdata)
            xdata = xdata[idx]
            ydata = ydata[idx]
            xerr = xerr[:,idx]
            yerr = yerr[:,idx]
            pe = pe[idx]

            # Limit how many points we show
            idx = [0]
            for l in range(len(xdata)):
                if xdata[l] - xdata[idx[-1]] > skip:
                    idx.append(l)

            # Plot medians
            p,=ax.plot(xdata, ydata-xdata, colors[k]+symbols[k])
            pgal.append(p)

            # Limit how many points we show
            idx = [0]
            for l in range(len(xdata)):
                if xdata[l] - xdata[idx[-1]] > skip1:
                    idx.append(l)

            # Plot 10th - 90th percentile
            ax.errorbar(xdata[idx], ydata[idx]-xdata[idx], 
                        xerr = xerr[:,idx], 
                        yerr = yerr[:,idx],
                        fmt='none', ecolor=colors[k])

        # Set limits
        ax.set_xlim(tra)
        ax.set_ylim(deltatra)

        # Add 1 to 1 line
        ax.plot(tra, [0,0], 'k--')

        # Add labels
        if i == j - 1:
            ax2 = ax.twinx()
            ax2.set_ylabel(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.
                           format(int(csfit[0][j]['msprior']),
                                  csfit[0][j]['tsprior']),
                           labelpad=10)
            ax2.set_yticks([])
            ax3 = ax.twiny()
            ax3.set_xlabel(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.
                           format(int(csfit[0][i]['msprior']),
                                  csfit[0][i]['tsprior']))
            ax3.set_xticks([])

        # Fix tick labels
        if i != 0:
            ax.set_yticklabels([])
        else:
            tcks = ax.get_yticks()
            if j == 1:
                ax.set_yticks(tcks[tcks == np.floor(tcks)])
            else:
                ax.set_yticks(tcks[tcks == np.floor(tcks)][:-1])
        if j != nprior-1:
            ax.set_xticklabels([])
        pos = ax.get_position()

# Fix spacing
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.07, left=0.07, 
                    right=0.95, top=0.95)

# Make common plot labels
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_xlabel(r'$\log\,T$ [yr]', labelpad=24)
ax.set_ylabel(r'$\Delta\,\log\,T$ [yr]', labelpad=18)

# Put thick box around fiducial column
ax = fig.add_subplot(1,nprior-1,1)
ax.patch.set_facecolor('None')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.spines['top'].set_linewidth(4)
ax.spines['bottom'].set_linewidth(4)
ax.spines['left'].set_linewidth(4)
ax.spines['right'].set_linewidth(4)
ax.set_xticklabels([])
ax.set_yticklabels([])

# Add legend
ax = fig.add_subplot(1,3,3)
ax.patch.set_facecolor('None')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.legend(pgal, gallabels, loc='upper left', numpoints=1)

# Save
plt.savefig('prior_age.pdf')
