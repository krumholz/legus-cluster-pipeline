# This script plots the marginal mass and age distributions for the
# population, with errors estimated from bootstrap resampling.

import numpy as np
from numpy.random import randint
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os.path as osp
from astropy.io import fits
from astropy.io import ascii as apyascii
import glob

# Input photometry files
photfiles = ['ngc7793e_cluster_legus_avgapcor_PadAGB_MWext_03Aug15.tab', 
             'ngc7793w_cluster_legus_avgapcor_PadAGB_MWext_04Aug15.tab',
             'ngc628e_cluster_legus_avgapcor_PadAGB_MWext_27Jun15.tab']
distmod = [27.68, 27.72, 29.98]

# Index of the nuclear cluster, so we can throw it out
nuc_cl = [321, 822, -1]

# Specify cluster_slug models to use
csfile = '/Users/krumholz/Data/legus/modp020_kroupalim_MW_phi0.50'
libdir = '/home/krumholz/pfs/cluster_slug/legus/legus-cluster-pipeline/bw0.1'
libname = 'pad_020_kroupa_MW'
galnames = ['ngc7793e', 'ngc7793w', 'ngc628e']
gallabels = ['NGC7793e', 'NGC7793w', 'NGC0628e']
msfid = 2.0
tsfid = 0.5
fidlib = 'modp020_kroupalim_MW_phi0.50'
extlib = 'modp020_kroupalim_SB_phi0.50'
z1lib = 'modp004_kroupalim_MW_phi0.50'
z2lib = 'modp008_kroupalim_MW_phi0.50'
imflib = 'modp020_chabrier_MW_phi0.50'
tracklib = 'Z0140v00_kroupalim_MW_phi0.50'
neblib = 'modp020_kroupalim_MW_phi0.50_noneb'
alllibs = [fidlib, imflib, extlib, neblib, tracklib, z1lib, z2lib]
runlabels = ['Fiducial', 'Chabrier', 'SB ext', 'No nebular',
             'Geneva', r'$Z=0.004$', r'$Z=0.008$' ]


# Go through photometry files to find high confidence clusters; also
# read the ydgrassil masses and ages
classidx = []
ra = []
dec = []
ygmass = []
ygage = []
for dmod, nucidx, pf in zip(distmod, nuc_cl, photfiles):

    # Read classification
    data = apyascii.read(pf)
    classification = data['col34']
    classidx.append(np.logical_and(classification > 0, classification < 3.5))

    # Filter out the nuclear cluster
    cid = list(data['col1'])
    if nucidx in cid:
        classidx[-1][cid.index(nucidx)] = False

    # Get RA and DEC to remove duplicates
    ra_tmp = np.array(data['col4'])
    dec_tmp = np.array(data['col5'])
    for r, d in zip(ra, dec):
        dup = np.logical_and(np.in1d(ra_tmp, r), np.in1d(dec_tmp, d))
        classidx[-1] = np.logical_and(classidx[-1], np.logical_not(dup))

    # Store
    ygage.append(data['col17'][classidx[-1]])
    ygmass.append(data['col20'][classidx[-1]])
    ra.append(ra_tmp[classidx[-1]])
    dec.append(dec_tmp[classidx[-1]])


# Put all the ygdrassil fits into a single array
ygmall=np.zeros(0)
ygtall=np.zeros(0)
for ygm, ygt in zip(ygmass, ygage):
    ygmall=np.append(ygmall, ygm)
    ygtall=np.append(ygtall, ygt)

# Read the posterior marginal PDFs from cluster_slug for various
# priors; again, strip the things that are not classified as
# well-detected clusters
csfit = []
for j in range(len(galnames)):

    # Get all output files for this galaxy
    files = glob.glob(
        osp.join(libdir,
                 galnames[j]+'_'+osp.basename(csfile) +
                 '_ms???_ts???_pdf_1D.fits'))
    csgal = []
    for i, f1 in enumerate(files):

        # Strip directory
        f = osp.basename(f1)

        # Read the extinction curve we're using
        ext = f.split('_')[3]

        # Read data
        hdulist = fits.open(f1)
        logm = hdulist[1].data['log_m']
        logt = hdulist[1].data['log_t']
        AV = hdulist[1].data['AV']
        mpdf = hdulist[2].data['Mass_PDF'][classidx[j]]
        tpdf = hdulist[2].data['Age_PDF'][classidx[j]]
        AVpdf = hdulist[2].data['AV_PDF'][classidx[j]]
        dist = hdulist[2].data['Phot_dist'][classidx[j]]
        dist_norm = hdulist[2].data['Phot_dist_norm'][classidx[j]]
        msprior = -float(f.split('ms')[1][:3])
        tsprior = -float(f.split('ts')[1][:3])
        hdulist.close()

        # Compute percentiles
        mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
        logm_pct = np.array([
            logm[np.argmax(np.greater(mpdfsum, 0.1), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.9), axis=1)]])
        tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
        logt_pct = np.array([
            logt[np.argmax(np.greater(tpdfsum, 0.1), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.9), axis=1)]])
        AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
        AV_pct = np.array([
            AV[np.argmax(np.greater(AVpdfsum, 0.1), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.9), axis=1)]])

        # Sum distributions to get whole population distribution
        mpdfsum = np.sum(mpdf, axis=0)
        mpdfsum = mpdfsum / (np.sum(mpdfsum)*(logm[1]-logm[0]))
        tpdfsum = np.sum(tpdf, axis=0)
        tpdfsum = tpdfsum / (np.sum(tpdfsum)*(logt[1]-logt[0]))
        AVpdfsum = np.sum(AVpdf, axis=0)
        AVpdfsum = AVpdfsum / (np.sum(AVpdfsum)*(AV[1]-AV[0]))

        # Save data
        csgal.append(
            { 'msprior' : msprior, 'tsprior' : tsprior,
              'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
              'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
              'AV_pct' : AV_pct, 'dist' : dist, 'dist_norm' : dist_norm,
              'ext' : ext, 'logm' : logm, 'logt' : logt,
              'AV' : AV, 'mpdfsum' : mpdfsum, 'tpdfsum' : tpdfsum,
              'AVpdfsum' : AVpdfsum })

    # Save
    csfit.append(csgal)

# Read the posterior marginal PDFs from cluster_slug for fixed priors
# but variations on other parameters
csfitmod = []
for j in range(len(galnames)):

    # Loop over libraries
    csgal = []
    for i in range(len(alllibs)):

        # Construct the file name
        f = osp.join(libdir, 
                     galnames[j]+'_'+alllibs[i]+
                     '_ms{:3.1f}_ts{:3.1f}_pdf_1D.fits'.format(msfid, tsfid))

        # Read data
        try:
            hdulist = fits.open(f)
        except IOError:
            continue
        logm = hdulist[1].data['log_m']
        logt = hdulist[1].data['log_t']
        AV = hdulist[1].data['AV']
        mpdf = hdulist[2].data['Mass_PDF'][classidx[j]]
        tpdf = hdulist[2].data['Age_PDF'][classidx[j]]
        AVpdf = hdulist[2].data['AV_PDF'][classidx[j]]
        dist = hdulist[2].data['Phot_dist'][classidx[j]]
        dist_norm = hdulist[2].data['Phot_dist_norm'][classidx[j]]
        hdulist.close()

        # Compute percentiles
        mpdfsum = np.cumsum(mpdf, axis=1)*(logm[1]-logm[0])
        logm_pct = np.array([
            logm[np.argmax(np.greater(mpdfsum, 0.1), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.25), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.5), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.75), axis=1)],
            logm[np.argmax(np.greater(mpdfsum, 0.9), axis=1)]])
        tpdfsum = np.cumsum(tpdf, axis=1)*(logt[1]-logt[0])
        logt_pct = np.array([
            logt[np.argmax(np.greater(tpdfsum, 0.1), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.25), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.5), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.75), axis=1)],
            logt[np.argmax(np.greater(tpdfsum, 0.9), axis=1)]])
        AVpdfsum = np.cumsum(AVpdf, axis=1)*(AV[1]-AV[0])
        AV_pct = np.array([
            AV[np.argmax(np.greater(AVpdfsum, 0.1), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.25), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.5), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.75), axis=1)],
            AV[np.argmax(np.greater(AVpdfsum, 0.9), axis=1)]])

        # Sum distributions to get whole population distribution
        mpdfsum = np.sum(mpdf, axis=0)
        mpdfsum = mpdfsum / (np.sum(mpdfsum)*(logm[1]-logm[0]))
        tpdfsum = np.sum(tpdf, axis=0)
        tpdfsum = tpdfsum / (np.sum(tpdfsum)*(logt[1]-logt[0]))
        AVpdfsum = np.sum(AVpdf, axis=0)
        AVpdfsum = AVpdfsum / (np.sum(AVpdfsum)*(AV[1]-AV[0]))

        # Save data
        csgal.append(
            { 'msprior' : msprior, 'tsprior' : tsprior,
              'mpdf' : mpdf, 'tpdf' : tpdf, 'AVpdf' : AVpdf,
              'logm_pct' : logm_pct, 'logt_pct' : logt_pct,
              'AV_pct' : AV_pct, 'dist' : dist, 'dist_norm' : dist_norm,
              'ext' : ext, 'logm' : logm, 'logt' : logt,
              'AV' : AV, 'mpdfsum' : mpdfsum, 'tpdfsum' : tpdfsum,
              'AVpdfsum' : AVpdfsum })

    # Save
    csfitmod.append(csgal)


# Put all clusters into a single array for convenience
ncltot = 0
for i in range(len(csfit)):
    ncltot = ncltot + len(csfit[i][0]['mpdf'])
mpdfall = np.zeros((len(csfit[0]), ncltot, csfit[0][0]['mpdf'].shape[1]))
tpdfall = np.zeros((len(csfit[0]), ncltot, csfit[0][0]['tpdf'].shape[1]))
ptr = 0
for i in range(len(csfit)):
    for j in range(len(csfit[i])):
        ncl = len(csfit[i][j]['mpdf'])
        mpdfall[j, ptr:ptr+ncl, :] = csfit[i][j]['mpdf']
        tpdfall[j, ptr:ptr+ncl, :] = csfit[i][j]['tpdf']
    ptr = ptr+ncl
mpdfall_mod = np.zeros((len(csfitmod[0]), ncltot, 
                        csfitmod[0][0]['mpdf'].shape[1]))
tpdfall_mod = np.zeros((len(csfitmod[0]), ncltot, 
                        csfitmod[0][0]['tpdf'].shape[1]))
ptr = 0
for i in range(len(csfitmod)):
    for j in range(len(csfitmod[i])):
        ncl = len(csfitmod[i][j]['mpdf'])
        mpdfall_mod[j, ptr:ptr+ncl, :] = csfitmod[i][j]['mpdf']
        tpdfall_mod[j, ptr:ptr+ncl, :] = csfitmod[i][j]['tpdf']
    ptr = ptr+ncl


# Get central estimates of population PDF, summed over all fields
mpdfsum = np.mean(mpdfall, axis=1)
tpdfsum = np.mean(tpdfall, axis=1)
mpdfsum_mod = np.mean(mpdfall_mod, axis=1)
tpdfsum_mod = np.mean(tpdfall_mod, axis=1)

# Do bootstrap
ntrial = 50000
mpdfsamp = np.zeros((mpdfall.shape[0], ntrial, mpdfall.shape[-1]))
tpdfsamp = np.zeros((tpdfall.shape[0], ntrial, tpdfall.shape[-1]))
mpdfsamp_mod = np.zeros((mpdfall_mod.shape[0], ntrial, mpdfall_mod.shape[-1]))
tpdfsamp_mod = np.zeros((tpdfall_mod.shape[0], ntrial, tpdfall_mod.shape[-1]))
for i in range(mpdfall.shape[0]):
    samp = randint(ncltot, size=ncltot*ntrial)
    for j in range(ntrial):
        mpdfsamp[i,j,:] \
            = np.mean(mpdfall[i, samp[ncltot*j:ncltot*(j+1)], :], axis=0)
        tpdfsamp[i,j,:] \
            = np.mean(tpdfall[i, samp[ncltot*j:ncltot*(j+1)], :], axis=0)
for i in range(mpdfall_mod.shape[0]):
    samp = randint(ncltot, size=ncltot*ntrial)
    for j in range(ntrial):
        mpdfsamp_mod[i,j,:] \
            = np.mean(mpdfall_mod[i, samp[ncltot*j:ncltot*(j+1)], :], axis=0)
        tpdfsamp_mod[i,j,:] \
            = np.mean(tpdfall_mod[i, samp[ncltot*j:ncltot*(j+1)], :], axis=0)
mpdfsamp = np.sort(mpdfsamp, axis=1)
tpdfsamp = np.sort(tpdfsamp, axis=1)
mpdfsamp_mod = np.sort(mpdfsamp_mod, axis=1)
tpdfsamp_mod = np.sort(tpdfsamp_mod, axis=1)

# Bin the yggdrasil ages and masses onto a uniform grid
medges=np.arange(2, 6.001, 0.5)
tedges=np.arange(5.99999, 11, 0.5)
mctr = 0.5*(medges[1:]+medges[:-1])
tctr = 0.5*(tedges[1:]+tedges[:-1])
mpdfyg = np.histogram(np.log10(ygmall), medges)[0]
tpdfyg = np.histogram(np.log10(ygtall), tedges)[0]
mpdfygerr = np.sqrt(mpdfyg)
tpdfygerr = np.sqrt(tpdfyg)
mnorm = (medges[1]-medges[0])*np.sum(mpdfyg)
tnorm = (tedges[1]-tedges[0])*np.sum(tpdfyg)
mpdfyg = mpdfyg / mnorm
tpdfyg = tpdfyg / tnorm
mpdfygerr = mpdfygerr / mnorm
tpdfygerr = tpdfygerr / tnorm

# Bin the yg masses and ages onto an adaptive grid
ygmall = np.sort(ygmall)
ygtall = np.sort(ygtall)
brks = np.linspace(0, len(ygmall)-1, 26, dtype='int')
medges = np.log10(ygmall[brks])
tedges = np.log10(ygtall[brks])
medges[0] = medges[0]/(1.0+1e-4)
medges[-1] = medges[-1]*(1.0+1e-4)
tedges[0] = tedges[0]/(1.0+1e-4)
tedges[-1] = tedges[-1]*(1.0+1e-4)
mctr_adapt = 0.5*(medges[1:]+medges[:-1])
tctr_adapt = 0.5*(tedges[1:]+tedges[:-1])
mpdfyg_adapt = (brks[1:]-brks[:-1]) / \
               (len(ygmall)*(medges[1:]-medges[:-1]))
tpdfyg_adapt = (brks[1:]-brks[:-1]) / \
               (len(ygtall)*(tedges[1:]-tedges[:-1]))
mpdfygerr_adapt = np.sqrt(brks[1:]-brks[:-1]) / \
                  (len(ygmall)*(medges[1:]-medges[:-1]))
tpdfygerr_adapt = np.sqrt(brks[1:]-brks[:-1]) / \
                  (len(ygtall)*(tedges[1:]-tedges[:-1]))

# Plot summed PDF of mass
fig = plt.figure(1, figsize=(6,12))
plt.clf()
logm = csfit[0][0]['logm']

# Central estimates for all models
plt.subplot(3,1,1)
colors_mod = ['k', 'r', 'y', 'b', 'g', 'c', 'm']
p=[]
for i in range(len(csfitmod[0])):
    p1,=plt.plot(logm, mpdfsum_mod[i,:], colors_mod[i], lw=2)
    p.append(p1)
#plt.errorbar(mctr_adapt, mpdfyg_adapt, mpdfygerr_adapt, fmt='none', 
#             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
#p1,=plt.plot(mctr_adapt, mpdfyg_adapt, 'o', ms=8, mfc='w', 
#             mec='k', mew=1.5)
#p1,=plt.plot(mctr, mpdfyg, 'o', mfc='#ff8888', ms=8)
#plt.errorbar(mctr, mpdfyg, mpdfygerr, fmt='none', elinewidth=2,
#             ecolor='#ff4444', capsize=4, capthick=2)
logmprior = 1.0/10.**csfit[0][0]['logm']
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
plt.plot(logm, logmprior, 'k--')
logmprior[:] = 1.0
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
plt.plot(logm, logmprior, 'k-.')
plt.xlim([2,6.0])
plt.ylim([1e-3, 3])
plt.yscale('log')
plt.gca().get_xaxis().set_ticklabels([])
plt.legend(p, runlabels, loc='lower left', prop = {'size':10})
plt.text(5.8, 1, 'Varying models', horizontalalignment='right')

# Central estimates for all sets of priors
colors = ['c', 'r', 'y', 'm', 'k', 'b', 'm']
plt.subplot(3,1,2)
p=[]
lab=[]
for i in range(len(csfit[0])):
        p1,=plt.plot(logm, mpdfsum[i,:], colors[i], lw=2)
        p.append(p1)
        lab.append(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.format(
            int(csfit[0][i]['msprior']),
            csfit[0][i]['tsprior']))
        if (csfit[0][i]['msprior'] == -msfid) and \
           (csfit[0][i]['tsprior'] == -tsfid):
            lab[-1] = lab[-1] + ' (Fiducial)'
#plt.errorbar(mctr_adapt, mpdfyg_adapt, mpdfygerr_adapt, fmt='none', 
#             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
#p1,=plt.plot(mctr_adapt, mpdfyg_adapt, 'o', ms=8, mfc='w', 
#             mec='k', mew=1.5)
#p1,=plt.plot(mctr, mpdfyg, 'o', mfc='#ff8888', ms=8)
#plt.errorbar(mctr, mpdfyg, mpdfygerr, fmt='none', elinewidth=2,
#             ecolor='#ff4444', capsize=4, capthick=2)
logmprior = 1.0/10.**csfit[0][0]['logm']
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
plt.plot(logm, logmprior, 'k--')
logmprior[:] = 1.0
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
plt.plot(logm, logmprior, 'k-.')
plt.yscale('log')
plt.xlim([2,6.0])
plt.ylim([1e-3, 3])
plt.gca().get_xaxis().set_ticklabels([])
plt.legend(p, lab, loc = 'lower left', prop = {'size':10})
plt.text(5.8, 1, 'Varying priors', horizontalalignment='right')


# Bootstrap resample confidence intervals for select priors
plt.subplot(3,1,3)
idx = [4, 5]
idxmod = [-1]
p=[]
lab=[]
plt.errorbar(mctr_adapt, mpdfyg_adapt, mpdfygerr_adapt, fmt='none', 
             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
p1,=plt.plot(mctr_adapt, mpdfyg_adapt, 'o', ms=8, mfc='w', 
             mec='k', mew=1.5, zorder=20)
p.append(p1)
lab.append('Yggdrasil (adaptive)')
p1,=plt.plot(mctr, mpdfyg, 'o', mfc='#ff8888', ms=8, mec='k', mew=1.5,
             zorder=20)
plt.errorbar(mctr, mpdfyg, mpdfygerr, fmt='none', elinewidth=2,
             ecolor='#ff4444', capsize=4, capthick=2)
p.append(p1)
lab.append('Yggdrasil (uniform)')
for i in idx:
    p1,=plt.plot(logm, mpdfsum[i,:], colors[i], lw=2)
    plt.fill_between(logm, mpdfsamp[i,int(ntrial*0.05),:], 
                     mpdfsamp[i,int(ntrial*0.95),:],
                     color=colors[i], alpha=0.25)
    p.append(p1)
    lab.append(r'Fiducial model, $\beta = {:d}$, $\gamma = {:4.1f}$'.format(
        int(csfit[0][i]['msprior']),
        csfit[0][i]['tsprior']))
for i in idxmod:
    p1,=plt.plot(logm, mpdfsum_mod[i,:], colors_mod[i], lw=2)
    plt.fill_between(logm, mpdfsamp_mod[i,int(ntrial*0.05),:], 
                     mpdfsamp_mod[i,int(ntrial*0.95),:],
                     color=colors_mod[i], alpha=0.25)
    p.append(p1)
    lab.append(runlabels[i] + ', fiducial prior')
logmprior[:] = 1.0
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
p1,=plt.plot(logm, logmprior, 'k-.')
p.append(p1)
lab.append(r'Prior $\beta = -1$')
logmprior = 1.0/10.**csfit[0][0]['logm']
logmprior = logmprior * mpdfsum[0,36]/logmprior[36]
p1,=plt.plot(logm, logmprior, 'k--')
p.append(p1)
lab.append(r'Prior $\beta = -2$')
plt.legend(p, lab, loc = 'lower left', prop = {'size':10},
           numpoints=1)
plt.yscale('log')
plt.xlim([2,6.0])
plt.ylim([1e-3, 3])
plt.xlabel('$\log\,M$ [$M_\odot$]')
plt.text(5.8, 1, 'Errors, comparison to Yggdrasil',
         horizontalalignment='right')

# Adjustment and overall y axis label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel('Population probability density', labelpad=30)
plt.subplots_adjust(wspace=0, hspace=0, bottom=0.07, top=0.96, right=0.95)
plt.savefig('mass_pop.pdf')


# Plot summed PDF of age
fig = plt.figure(2, figsize=(6,12))
plt.clf()
logt = csfit[0][0]['logt']

# PDFs for all models
plt.subplot(3,1,1)
p=[]
lab=[]
for i in range(len(csfitmod[0])):
    p1,=plt.plot(logt, tpdfsum_mod[i,:], colors_mod[i], lw=2)
    p.append(p1)
#plt.errorbar(tctr_adapt, tpdfyg_adapt, tpdfygerr_adapt, fmt='none', 
#             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
#p1,=plt.plot(tctr_adapt, tpdfyg_adapt, 'o', ms=8, mfc='w', 
#             mec='k', mew=1.5)
#p1,=plt.plot(tctr, tpdfyg, 'o', mfc='#ff8888', ms=8)
#plt.errorbar(tctr, tpdfyg, tpdfygerr, fmt='none', elinewidth=2,
#             ecolor='#ff4444', capsize=4, capthick=2)
logtprior = 10.**logt
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k--')
logtprior[logt >= 6.5] = 10.**(0.5*(logt[logt > 6.5]-6.5))
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k-.')
logtprior[:] = 1.0
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k:')
plt.gca().get_xaxis().set_ticklabels([])
plt.yscale('log')
plt.xlim([5, 9.5])
plt.ylim([1e-4, 3])
plt.text(8.3, 1, 'Varying models')
plt.legend(p, runlabels, loc = 'lower center', 
           prop = {'size':10})

# PDFs for all priors
plt.subplot(3,1,2)
p=[]
lab=[]
for i in range(len(csfit[0])):
    p1,=plt.plot(logt, tpdfsum[i,:], colors[i], lw=2)
    p.append(p1)
    lab.append(r'$\beta = {:d}$, $\gamma = {:4.1f}$'.format(
        int(csfit[0][i]['msprior']),
        csfit[0][i]['tsprior']))
#plt.errorbar(tctr_adapt, tpdfyg_adapt, tpdfygerr_adapt, fmt='none', 
#             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
#p1,=plt.plot(tctr_adapt, tpdfyg_adapt, 'o', ms=8, mfc='w', 
#             mec='k', mew=1.5)
#p1,=plt.plot(tctr, tpdfyg, 'o', mfc='#ff8888', ms=8)
#plt.errorbar(tctr, tpdfyg, tpdfygerr, fmt='none', elinewidth=2,
#             ecolor='#ff4444', capsize=4, capthick=2)
logtprior = 10.**logt
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k--')
logtprior[logt >= 6.5] = 10.**(0.5*(logt[logt > 6.5]-6.5))
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k-.')
logtprior[:] = 1.0
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
plt.plot(logt, logtprior, 'k:')
plt.gca().get_xaxis().set_ticklabels([])
plt.yscale('log')
plt.xlim([5, 9.5])
plt.ylim([1e-4, 3])
plt.text(8.3, 1, 'Varying priors')
plt.legend(p, lab, loc = 'lower center', prop = {'size':10})

# Bootstrap resample confidence intervals for select priors
plt.subplot(3,1,3)
idx = [4, 5]
idxmod = [-1]
p=[]
lab=[]
plt.errorbar(tctr_adapt, tpdfyg_adapt, tpdfygerr_adapt, fmt='none', 
             elinewidth=1.5, ecolor='k', capsize=4, capthick=1.5)
p1,=plt.plot(tctr_adapt, tpdfyg_adapt, 'o', ms=8, mfc='w', 
             mec='k', mew=1.5, zorder=20)
p.append(p1)
lab.append('Yggdrasil (adaptive)')
p1,=plt.plot(tctr, tpdfyg, 'o', mfc='#ff8888', ms=8, mec='k', mew=1.5, 
             zorder=20)
plt.errorbar(tctr, tpdfyg, tpdfygerr, fmt='none', elinewidth=2,
             ecolor='#ff4444', capsize=4, capthick=2)
p.append(p1)
lab.append('Yggdrasil (uniform)')
for i in idx:
    p1,=plt.plot(logt, tpdfsum[i,:], colors[i], lw=2)
    plt.fill_between(logt, tpdfsamp[i,int(ntrial*0.05),:], 
                     tpdfsamp[i,int(ntrial*0.95),:],
                     color=colors[i], alpha=0.25)
    p.append(p1)
    lab.append(r'Fid. mod., $\beta = {:d}$, $\gamma = {:4.1f}$'.format(
        int(csfit[0][i]['msprior']),
        csfit[0][i]['tsprior']))
for i in idxmod:
    p1,=plt.plot(logt, tpdfsum_mod[i,:], colors_mod[i], lw=2)
    p.append(p1)
    plt.fill_between(logt, tpdfsamp_mod[i,int(ntrial*0.05),:], 
                     tpdfsamp_mod[i,int(ntrial*0.95),:],
                     color=colors_mod[i], alpha=0.25)
    lab.append(runlabels[i] + ', fiducial prior')
logtprior = 10.**logt
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
p1,=plt.plot(logt, logtprior, 'k--')
p.append(p1)
lab.append(r'Prior $\gamma = 0$')
logtprior[logt >= 6.5] = 10.**(0.5*(logt[logt > 6.5]-6.5))
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
p1,=plt.plot(logt, logtprior, 'k-.')
p.append(p1)
lab.append(r'Prior $\gamma = -0.5$')
logtprior[:] = 1.0
logtprior[logt < 6.5] = 10.**(logt[logt < 6.5]-6.5)
logtprior = 1.5 * logtprior * tpdfsum[0,37]/logtprior[37]
p1,=plt.plot(logt, logtprior, 'k:')
p.append(p1)
lab.append(r'Prior $\gamma = -1$')
plt.yscale('log')
plt.xlabel('$\log\,T$ [yr]')
plt.xlim([5, 9.5])
plt.ylim([1e-4, 3])
plt.text(8.3, 0.7, 'Errors, comp.\nto Yggdrasil')
plt.legend(p, lab, loc = 'lower center', prop = {'size':10},
           numpoints=1)


# Adjustment and overall y axis label
ax = fig.add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_facecolor('None')
ax.set_xticklabels([])
ax.set_yticklabels([])
ax.set_ylabel('Population probability density', labelpad=30)
plt.subplots_adjust(wspace=0, hspace=0, bottom=0.07, top=0.96, right=0.95)
plt.savefig('age_pop.pdf')
